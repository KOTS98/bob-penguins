package apalacios.pfg.util;

public enum LastScreen {
    START, PROFILES, ONEPLAYER, MULTIPLAYER
}
