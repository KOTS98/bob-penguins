package apalacios.pfg.weapons;

import apalacios.pfg.characters.Proyectile;
import apalacios.pfg.characters.Weapon;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.Direction;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Weapon6 extends Weapon {
    private double lastShot;

    public Weapon6(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 6, -1);
        nMunition = 1;

        shootSpeed = 1000;
        lastShot = System.currentTimeMillis();
        shootSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/camera.ogg"));
    }

    @Override
    public void shoot() {
        double currentShot = System.currentTimeMillis();
        if (lastShot - currentShot < -shootSpeed) {
            lastShot = currentShot;
            shootSound.play(Config.soundVolume);
            if (direction == Direction.RIGHT)
                new Proyectile(getX() + (getWidth() / 2) + 50, getY() + 15, getStage(), 300, direction,
                        false, 6, 50);
            else
                new Proyectile(getX() + (getWidth() / 2) - 130, getY() + 15, getStage(), 300, direction,
                        false, 6, 50);
        }
    }

    @Override
    public void stopShooting() {

    }

    @Override
    public void reload() {

    }
}
