package apalacios.pfg.characters;

import apalacios.pfg.controllers.ControllerProfile;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.DeadType;
import apalacios.pfg.util.Direction;
import apalacios.pfg.weapons.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class Player extends Mobile {

    // Visual
    private Animation rightStand;
    private Animation leftStand;
    private Animation rightWalk;
    private Animation leftWalk;
    private Animation rightKick;
    private Animation leftKick;
    private Animation rightWing;
    private Animation leftWing;
    private Animation rightWingAlt;
    private Animation leftWingAlt;
    private Animation rightSkin;
    private Animation leftSkin;
    private Animation rightSkinAlt;
    private Animation leftSkinAlt;
    private String playerName;
    private Label nameLabel;

    private BaseActor body;
    private BaseActor wing;
    private BaseActor skin;
    private int nColor;
    private int nSkin;
    private BaseActor feetSensorL;
    private BaseActor feetSensorR;
    private BaseActor kickBox;

    private boolean isJumping;
    private boolean trampolineJumping;
    private boolean isWalking;
    private boolean isKicking;
    private long kickStart;
    private boolean wantGoDown;
    private boolean alive;
    private boolean getingKicked;

    private Direction direction;

    private Weapon weapon;
    private boolean wantsToGrab;
    private boolean grabing;

    // Físicas
    private float walkAcceleration;
    private float walkDeceleration;
    private float maxHorizontalSpeed;
    private float gravity;
    private boolean gravityEnabled;
    private boolean gravityLock;
    private long gravityLockStart;
    private boolean onGround;
    private boolean onPlatform;
    private float maxVerticalSpeedUp;
    private float maxVerticalSpeedDown;

    // Mando (Controller)
    private int nPlayer;
    private boolean CPU;
    private Controller controller;
    private ControllerProfile controllerProfile;
    private int place;
    private boolean inVictoryScreen;
    private boolean crushed;

    // Sonidos
    private Sound deathSound;

    /**
     * Constructor de la calse, inicializar los parámetros de la misma
     *
     * @param posX  Representa su posición respecto a la pantalla en el eje horizontal
     * @param posY  Representa su posición respecto a la pantalla en el eje vertical
     * @param stage El actor (independientemente de lo que sea), sera añadido al stage,
     */
    public Player(float posX, float posY, Stage stage, int nColor, int nSkin, boolean CPU) {
        super(posX, posY, stage);
        setName("player");

        body = new BaseActor(getX() + 19, getY() + 47, getStage());
        wing = new BaseActor(getX() + 19, getY() + 47, getStage());
        skin = new BaseActor(getX() + 19, getY() + 47, getStage());
        this.nSkin = nSkin;
        this.nColor = nColor;
        feetSensorL = new BaseActor(getX() + 15, getY() + 2, getStage());
        feetSensorL.setSize(7, 7);
        feetSensorL.setBoundaryRectangle();
        feetSensorR = new BaseActor(getX() + 26, getY() + 2, getStage());
        feetSensorR.setSize(7, 7);
        feetSensorR.setBoundaryRectangle();
        kickBox = new BaseActor(getX(), getY(), getStage());
        kickBox.setSize(34, 33);
        kickBox.setBoundaryRectangle();

        setSize(39, 75);
        setBoundaryRectangle();

        isJumping = false;
        trampolineJumping = false;
        isWalking = false;
        isKicking = false;
        kickStart = System.currentTimeMillis();
        wantGoDown = false;
        direction = Direction.RIGHT;
        wantsToGrab = false;
        grabing = false;
        alive = true;
        getingKicked = false;

        maxHorizontalSpeed = 300;
        walkAcceleration = 5;
        walkDeceleration = 5;
        gravity = 10;
        gravityEnabled = true;
        gravityLock = false;
        gravityLockStart = System.currentTimeMillis();
        onGround = false;
        onPlatform = false;
        maxVerticalSpeedUp = 1000;
        maxVerticalSpeedDown = 500;


        rightStand = body.loadTexture("src/apalacios/pfg/assets/characters/pj" + nColor + "/pj" + nColor
                + "_stand_right.png");
        leftStand = body.loadTexture("src/apalacios/pfg/assets/characters/pj" + nColor + "/pj" + nColor
                + "_stand_left.png");
        rightWalk = body.loadAnimationFromSheet("src/apalacios/pfg/assets/characters/pj" + nColor + "/pj"
                        + nColor + "_walk_right.png",
                1, 8, 0.1f, true);
        leftWalk = body.loadAnimationFromSheet("src/apalacios/pfg/assets/characters/pj" + nColor + "/pj"
                        + nColor + "_walk_left.png",
                1, 8, 0.1f, true);
        rightKick = body.loadTexture("src/apalacios/pfg/assets/characters/pj" + nColor + "/pj" + nColor
                + "_kick_right.png");
        leftKick = body.loadTexture("src/apalacios/pfg/assets/characters/pj" + nColor + "/pj" + nColor
                + "_kick_left.png");
        rightWing = wing.loadTexture("src/apalacios/pfg/assets/characters/pj" + nColor + "/aleta_" + nColor
                + "_right.png");
        leftWing = wing.loadTexture("src/apalacios/pfg/assets/characters/pj" + nColor + "/aleta_" + nColor
                + "_left.png");
        rightWingAlt = wing.loadTexture("src/apalacios/pfg/assets/characters/pj" + nColor + "/aleta_" + nColor
                + "_alt_right.png");
        leftWingAlt = wing.loadTexture("src/apalacios/pfg/assets/characters/pj" + nColor + "/aleta_" + nColor
                + "_alt_left.png");
        if (nSkin != 0) {
            rightSkin = skin.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" + nSkin + "/right.png");
            leftSkin = skin.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" + nSkin + "/left.png");
            rightSkinAlt = skin.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" + nSkin + "/right_alt.png");
            leftSkinAlt = skin.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" + nSkin + "/left_alt.png");
        }

        body.setAnimation(rightStand);

        weapon = null;

        // Mando (Controller)
        nPlayer = 0;
        this.CPU = CPU;
        place = 0;
        inVictoryScreen = false;
        crushed = false;

        // Sonido
        deathSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/muerte.ogg"));
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean isOnGround() {
        return onGround;
    }

    public void setMaxHorizontalSpeed(float maxHorizontalSpeed) {
        this.maxHorizontalSpeed = maxHorizontalSpeed;
    }

    public boolean isKickingNow() {
        return isKicking;
    }

    public boolean isWantsToGrab() {
        return wantsToGrab;
    }

    public boolean isOnPlatform() {
        return onPlatform;
    }

    public void setOnPlatform(boolean onPlatform) {
        this.onPlatform = onPlatform;
    }

    public boolean isAlive(){
        return alive;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public int getnColor() {
        return nColor;
    }

    public void setControllerProfile(ControllerProfile controllerProfile) {
        this.controllerProfile = controllerProfile;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public void setPlayerName(String playerName, Stage stage) {
        this.playerName = playerName;
        nameLabel = new Label(playerName, BaseGame.styleNormal);
        stage.addActor(nameLabel);
        nameLabel.setPosition(body.getX() + (body.getWidth() / 2), 100);
    }

    public String getPlayerName() {
        return playerName;
    }

    public ControllerProfile getControllerProfile() {
        return controllerProfile;
    }

    public int getnSkin() {
        return nSkin;
    }

    public void setWeapon(int nWeapon) {
        if (direction == Direction.RIGHT) {
            wing.setAnimation(rightWingAlt);
            if (nSkin != 0)
                skin.setAnimation(rightSkinAlt);
        } else {
            wing.setAnimation(leftWingAlt);
            if (nSkin != 0)
                skin.setAnimation(leftSkinAlt);
        }

        switch (nWeapon) {
            case 0:
                this.weapon = new CrateWeapon(getX(), getY(), getStage());
                break;
            case 1:
                this.weapon = new Weapon1(getX(), getY(), getStage());
                break;
            case 2:
                this.weapon = new Weapon2(getX(), getY(), getStage());
                break;
            case 3:
                this.weapon = new Weapon3(getX(), getY(), getStage());
                break;
            case 4:
                this.weapon = new Weapon4(getX(), getY(), getStage());
                break;
            case 5:
                this.weapon = new Weapon5(getX(), getY(), getStage());
                break;
            case 6:
                this.weapon = new Weapon6(getX(), getY(), getStage());
                break;
            case 7:
                this.weapon = new Weapon7(getX(), getY(), getStage());
                break;
            case 8:
                this.weapon = new Weapon8(getX(), getY(), getStage());
                break;
            case 9:
                this.weapon = new Weapon9(getX(), getY(), getStage());
                break;
            case 10:
                this.weapon = new Weapon10(getX(), getY(), getStage());
                break;
        }
        weapon.setDirection(direction);
    }

    public BaseActor getFeetSensorL() {
        return feetSensorL;
    }

    public BaseActor getFeetSensorR() {
        return feetSensorR;
    }

    public BaseActor getKickBox() {
        return kickBox;
    }

    public Vector2 getVelocityVector() {
        return velocityVec;
    }

    public boolean isGravityLock() {
        return !gravityLock;
    }

    public void setGravityEnabled(boolean gravityEnabled) {
        this.gravityEnabled = gravityEnabled;
    }

    public int getnPlayer() {
        return nPlayer;
    }

    public void setnPlayer(int nPlayer) {
        this.nPlayer = nPlayer;
    }

    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public boolean isCPU() {
        return CPU;
    }

    public void setInVictoryScreen(boolean inVictoryScreen) {
        this.inVictoryScreen = inVictoryScreen;
    }

    public void moveRight() {
        if (!isKicking && (!isWalking || isCPU())) {
            isWalking = true;
            direction = Direction.RIGHT;
            body.setAnimation(rightWalk);
            if (weapon == null) {
                wing.setAnimation(rightWing);
            } else {
                wing.setAnimation(rightWingAlt);
                weapon.setDirection(direction);
            }
            if (nSkin != 0) {
                if (weapon == null) {
                    skin.setAnimation(rightSkin);
                } else {
                    skin.setAnimation(rightSkinAlt);
                }
            }
        }
    }

    public void moveLeft() {
        if (!isKicking && (!isWalking || isCPU())) {
            isWalking = true;
            direction = Direction.LEFT;
            body.setAnimation(leftWalk);
            if (weapon == null) {
                wing.setAnimation(leftWing);
            } else {
                wing.setAnimation(leftWingAlt);
                weapon.setDirection(direction);
            }
            if (nSkin != 0) {
                if (weapon == null) {
                    skin.setAnimation(leftSkin);
                } else {
                    skin.setAnimation(leftSkinAlt);
                }
            }
        }
    }

    public void decelerate() {
        isWalking = false;
        if (direction == Direction.RIGHT) {
            body.setAnimation(rightStand);
        } else {
            body.setAnimation(leftStand);
        }
    }

    public void jump() {
        if (isOnGround()) {
            isJumping = true;
        }
    }

    public void trampolineJump() {
        trampolineJumping = true;
    }

    public void goDown(boolean wantGoDown) {
        this.wantGoDown = wantGoDown;
    }

    public void grabItem() {
        if (!grabing && weapon == null)
            grabing = true;
    }

    public void throwItem() {
        if (weapon != null) {
            weapon.drop();
            weapon.remove();
            weapon = null;
            if (direction == Direction.RIGHT) {
                wing.setAnimation(rightWing);
                if (nSkin != 0)
                    skin.setAnimation(rightSkin);
            } else {
                wing.setAnimation(leftWing);
                if (nSkin != 0)
                    skin.setAnimation(leftSkin);
            }
        }
    }

    public void shoot() {
        if (weapon == null) {
            if (onGround && !isKicking) {
                long currentKickTime = System.currentTimeMillis();
                if (kickStart - currentKickTime < -1000){
                    kickStart = System.currentTimeMillis();
                    isKicking = true;
                    isWalking = false;
                    if (direction == Direction.RIGHT) {
                        body.setAnimation(rightKick);
                    } else {
                        body.setAnimation(leftKick);
                    }
                }
            }
        } else {
            if (!(weapon instanceof CrateWeapon)) {
                weapon.shoot();
            }
        }
    }

    public void stopShooting() {
        if (weapon != null) {
            if (!(weapon instanceof CrateWeapon)) {
                weapon.stopShooting();
            }
        }
    }

    public void reciveKick(Direction direction) {
        this.direction = direction;
        getingKicked = true;
        throwItem();
    }

    public void die(DeadType deadType) {
        deathSound.play(1);
        alive = inVictoryScreen;
        throwItem();

        if (deadType == DeadType.CRUSHED) {
            setScaleY(0.5f);
            setScaleX(2);
            body.setScaleY(0.5f);
            body.setScaleX(2);
            wing.setScaleY(0.5f);
            wing.setScaleX(2);
            skin.setScaleY(0.5f);
            skin.setScaleX(2);
            if (inVictoryScreen) {
                crushed = true;
                setPosition(getX(), getY() - 37);
            } else {
                setPosition(getX(), getY() - 25);
            }
        }

        if (!inVictoryScreen) {
            feetSensorR.remove();
            feetSensorL.remove();
            kickBox.remove();
            body.addAction(Actions.fadeOut(1));
            body.addAction(Actions.after(Actions.removeActor()));
            wing.addAction(Actions.fadeOut(1));
            wing.addAction(Actions.after(Actions.removeActor()));
            skin.addAction(Actions.fadeOut(1));
            skin.addAction(Actions.after(Actions.removeActor()));
            addAction(Actions.fadeOut(1));
            addAction(Actions.after(Actions.removeActor()));

            if (nameLabel != null) {
                nameLabel.addAction(Actions.fadeOut(1));
                nameLabel.addAction(Actions.after(Actions.removeActor()));
            }
        }
    }

    @Override
    public void setScale(float scale) {
        body.setScale(scale);
        wing.setScale(scale);
        skin.setScale(scale);
    }

    @Override
    public void act(float dt) {

        // Aceleración y deceleración
        if (isWalking && !isKicking) {
            if (direction == Direction.RIGHT) {
                accelerationVec.add(walkAcceleration, 0);
                if (velocityVec.x >= maxHorizontalSpeed) {
                    accelerationVec.x = 0;
                }
            } else {
                accelerationVec.add(-walkAcceleration, 0);
                if (velocityVec.x <= -maxHorizontalSpeed) {
                    accelerationVec.x = 0;
                }
            }
        } else {
            if (direction == Direction.RIGHT) {
                accelerationVec.add(-walkDeceleration, 0);
                if (velocityVec.x < 0) {
                    accelerationVec.x = 0;
                    velocityVec.x = 0;
                }
            } else {
                accelerationVec.add(walkDeceleration, 0);
                if (velocityVec.x > 0) {
                    accelerationVec.x = 0;
                    velocityVec.x = 0;
                }
            }
        }

        // Simular gravedad
        if (gravityEnabled) {
            accelerationVec.add(0, -gravity);
        } else {
            if (gravityLock) {
                accelerationVec.add(0, -gravity);
            } else {
                accelerationVec.y = 0;
                velocityVec.y = 0;
            }
        }

        // Salto
        if (isJumping) {
            if (wantGoDown) {
                gravityLockStart = System.currentTimeMillis();
                gravityLock = true;
                isJumping = false;
                wantGoDown = false;
            } else {
                accelerationVec.add(0, 110);
            }
        }

        if (trampolineJumping) {
            accelerationVec.add(0, 200);
        }

        if (gravityLock) {
            if (gravityLockStart - System.currentTimeMillis() < -300) {
                gravityLock = false;
            }
        }

        if (isKicking) {
            if (kickStart - System.currentTimeMillis() < -500) {
                isKicking = false;
                if (direction == Direction.RIGHT) {
                    body.setAnimation(rightStand);
                } else {
                    body.setAnimation(leftStand);
                }
            }
        }

        if (getingKicked) {
            getingKicked = false;
            accelerationVec.add(0, 20);
            if (direction == Direction.RIGHT)
                accelerationVec.add(20, 0);
            else
                accelerationVec.add(-20, 0);
        }

        if (wantsToGrab) {
            wantsToGrab = false;
        }

        if (grabing && weapon == null) {
            wantsToGrab = true;
            grabing = false;
        }

        if (!alive) {
            accelerationVec.set(0, 0);
            velocityVec.set(0, 0);
        }

        velocityVec.add(accelerationVec.x, accelerationVec.y);

        velocityVec.x = MathUtils.clamp(velocityVec.x, -maxHorizontalSpeed, maxHorizontalSpeed);

        if (isJumping)
            velocityVec.y = MathUtils.clamp(velocityVec.y, -maxVerticalSpeedDown, 110);
        else if (trampolineJumping)
            velocityVec.y = MathUtils.clamp(velocityVec.y, -maxVerticalSpeedDown, 200);
        else
            velocityVec.y = MathUtils.clamp(velocityVec.y, -maxVerticalSpeedDown, maxVerticalSpeedUp);

        isJumping = false;
        trampolineJumping = false;

        moveBy(Math.round(velocityVec.x * dt), Math.round(velocityVec.y * dt));

        // Ajustar gráficos y sensores de pies
        if (crushed) {
            body.centerAtPosition(getX() + 19, getY());
            wing.centerAtPosition(getX() + 19, getY());
            skin.centerAtPosition(getX() + 19, getY());
        } else {
            body.centerAtPosition(getX() + 19, getY() + 47);
            wing.centerAtPosition(getX() + 19, getY() + 47);
            skin.centerAtPosition(getX() + 19, getY() + 47);
        }
        if (weapon != null)
            weapon.centerAtPosition(getX() + 19, getY() + 47);
        feetSensorL.centerAtPosition(getX() + 15, getY() + 2);
        feetSensorR.centerAtPosition(getX() + 26, getY() + 2);
        if (direction == Direction.RIGHT)
            kickBox.centerAtPosition(getX() + 37, getY() + 15);
        else
            kickBox.centerAtPosition(getX(), getY() + 15);
        if (nameLabel != null) {
            if (inVictoryScreen) {
                nameLabel.setPosition(body.getX() + (body.getWidth() / 2) - (nameLabel.getWidth() / 2),
                        body.getY() + 150);
            } else {
                nameLabel.setPosition(body.getX() + (body.getWidth() / 2) - (nameLabel.getWidth() / 2),
                        body.getY() + 100);
            }
        }
    }
}