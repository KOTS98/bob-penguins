package apalacios.pfg.game;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;


/**
 * Lanzador del juego, se encarga de iniciar el juego
 *
 * @author KOTS98
 */
public class Launcher {

    /**
     * Método principal del juego, ejecuta el juego en una ventana
     *
     * @param args .
     */
    public static void main (String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Bob Penguins - by KOTS98";
        cfg.width = 1920;
        cfg.height = 1080;
        new LwjglApplication(new PFGGame(), cfg);
    }
}
