package apalacios.pfg.util;

import apalacios.pfg.controllers.ControllerProfile;

import java.io.Serializable;
import java.util.ArrayList;

public class Profile implements Serializable {
    private String name;
    private ArrayList<ControllerProfile> controllerProfiles;

    public Profile(String name) {
        this.name = name;
        controllerProfiles = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void addControllerProfile(ControllerProfile newControllerProfile) {
        for (int i = 0; i < controllerProfiles.size(); i++) {
            if (controllerProfiles.get(i).getControllerType().equalsIgnoreCase(newControllerProfile.getControllerType())) {
                controllerProfiles.remove(i);
                break;
            }
        }
        controllerProfiles.add(newControllerProfile);
        System.out.println("[ OK ] Perfil de mando \"" + newControllerProfile.getControllerType() + "\" anadido con " +
                "exito al perfil \"" + name + "\"!");
    }

    public ArrayList<ControllerProfile> getControllerProfiles() {
        return controllerProfiles;
    }
}
