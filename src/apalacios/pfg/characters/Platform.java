package apalacios.pfg.characters;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Platform extends BaseActor {
    /**
     * Constructor de la calse, inicializar los parámetros de la misma
     *
     * @param posX  Representa su posición respecto a la pantalla en el eje horizontal
     * @param posY  Representa su posición respecto a la pantalla en el eje vertical
     * @param stage El actor (independientemente de lo que sea), sera añadido al stage,
     */
    public Platform(float posX, float posY, float width, float height, Stage stage) {
        super(posX, posY, stage);
        setSize(width, height);
        setBoundaryRectangle();
    }
}
