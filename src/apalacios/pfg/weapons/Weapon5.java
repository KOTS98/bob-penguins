package apalacios.pfg.weapons;

import apalacios.pfg.characters.Proyectile;
import apalacios.pfg.characters.Weapon;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.Direction;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.Random;

public class Weapon5 extends Weapon {
    private boolean firing;
    private float fireTime;
    private double lastShot;

    public Weapon5(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 5, -1);
        if (nMunition == -1)
            nMunition = 10;

        shootSpeed = 500;
        firing = false;
        fireTime = 0;
        lastShot = System.currentTimeMillis();
        shootSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/pistola.ogg"));
    }

    @Override
    public void shoot() {
        double currentShot = System.currentTimeMillis();
        if (lastShot - currentShot < -shootSpeed) {
            lastShot = currentShot;
            if (nMunition > 0) {
                firing = true;
                shootSound.play(Config.soundVolume);
                nMunition--;
            } else {
                noAmmoSound.play(Config.soundVolume);
            }
        }
    }

    @Override
    public void stopShooting() {

    }

    @Override
    public void reload() {

    }

    @Override
    public void act(float dt) {
        super.act(dt);
        if (firing) {
            if (direction == Direction.RIGHT)
                new Proyectile(getX() + (getWidth() / 2) + 40, getY() + (new Random().nextInt(10) + 1) + 50,
                        getStage(), 700, direction, true, 5, 1500);
            else
                new Proyectile(getX() + (getWidth() / 2) - 40, getY() + (new Random().nextInt(10) + 1) + 50,
                        getStage(), 700, direction, true, 5, 1500);
            fireTime += dt;
        }
        if (fireTime > 0.5f) {
            fireTime = 0;
            firing = false;
        }
    }
}
