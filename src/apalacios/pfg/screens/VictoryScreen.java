package apalacios.pfg.screens;

import apalacios.pfg.characters.*;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.game.PFGGame;
import apalacios.pfg.util.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.ArrayList;
import java.util.Collections;

public class VictoryScreen extends BaseGamepadScreen {

    // Jugadores
    private Player player1;
    private Player player2;
    private Player player3;
    private Player player4;

    // Pilares
    private Solid column1;
    private Solid column1Collider;
    private Label column1Label;

    private Solid column2;
    private Solid column2Collider;
    private Label column2Label;

    private Solid column3;
    private Solid column3Collider;
    private Label column3Label;

    private Solid column4;
    private Solid column4Collider;
    private Label column4Label;

    // Puntuaciones
    private boolean scoresShown;
    private int currentScreenScore;

    private BaseActor p1Color;
    private BaseActor p2Color;
    private BaseActor p3Color;
    private BaseActor p4Color;

    private BaseActor p1Skin;
    private BaseActor p2Skin;
    private BaseActor p3Skin;
    private BaseActor p4Skin;

    private Label p1Label;
    private Label p2Label;
    private Label p3Label;
    private Label p4Label;

    private Label p1ScoreLabel;
    private Label p2ScoreLabel;
    private Label p3ScoreLabel;
    private Label p4ScoreLabel;

    private long countDownTimer;
    private int currentScore;
    private int waitTime;
    private boolean canContinue;

    VictoryScreen(Player player1, Player player2, Player player3, Player player4) {

        BaseActor background = new BaseActor(0, 0, mainStage);
        background.loadTexture("src/apalacios/pfg/assets/background/victory_screen_bg.png");

        this.player1 = new Player(-50, -50, mainStage, player1.getnColor(), player1.getnSkin(),
                player1.isCPU());
        this.player1.setPosition(240 - (player1.getWidth() / 2), 365);
        this.player1.setPlayerName(player1.getPlayerName(), uiStage);
        this.player1.setController(player1.getController());
        this.player1.setControllerProfile(player1.getControllerProfile());
        this.player1.setScale(1.5f);
        this.player1.setInVictoryScreen(true);
        this.player1.setnPlayer(player1.getnPlayer());

        this.player2 = new Player(-50, -50, mainStage, player2.getnColor(), player2.getnSkin(),
                player2.isCPU());
        this.player2.setPosition(720 - (player2.getWidth() / 2), 365);
        this.player2.setPlayerName(player2.getPlayerName(), uiStage);
        this.player2.setController(player2.getController());
        this.player2.setControllerProfile(player2.getControllerProfile());
        this.player2.setScale(1.5f);
        this.player2.setInVictoryScreen(true);
        this.player2.setnPlayer(player2.getnPlayer());

        if (GameScore.nPlayers >= 3) {
            this.player3 = new Player(-50, -50, mainStage, player3.getnColor(), player3.getnSkin(),
                    player3.isCPU());
            this.player3.setPosition(1200 - (player3.getWidth() / 2), 365);
            this.player3.setPlayerName(player3.getPlayerName(), uiStage);
            this.player3.setController(player3.getController());
            this.player3.setControllerProfile(player3.getControllerProfile());
            this.player3.setScale(1.5f);
            this.player3.setInVictoryScreen(true);
            this.player3.setnPlayer(player3.getnPlayer());
        }

        if (GameScore.nPlayers == 4) {
            this.player4 = new Player(-50, -50, mainStage, player4.getnColor(), player4.getnSkin(),
                    player4.isCPU());
            this.player4.setPosition(1680 - (player4.getWidth() / 2), 365);
            this.player4.setPlayerName(player4.getPlayerName(), uiStage);
            this.player4.setController(player4.getController());
            this.player4.setControllerProfile(player4.getControllerProfile());
            this.player4.setScale(1.5f);
            this.player4.setInVictoryScreen(true);
            this.player4.setnPlayer(player4.getnPlayer());
        }

        // Pilares
        column1 = new Solid(-50, 0, 240, 347, mainStage);
        column1.loadTexture("src/apalacios/pfg/assets/world_elements/pilar.png");
        column1.setPosition(240 - (column1.getWidth() / 2), 0);
        column1Collider = new Solid(0, 0, 240, 50, mainStage);
        column1Label = new Label("0", BaseGame.styleBig);
        column1Label.setPosition(240 - (column1Label.getWidth() / 2), column1.getY() + column1.getHeight() - 105);
        column1Label.setText("");
        column1Label.addAction(Actions.fadeOut(0));
        uiStage.addActor(column1Label);

        column2 = new Solid(-50, 0, 240, 347, mainStage);
        column2.loadTexture("src/apalacios/pfg/assets/world_elements/pilar.png");
        column2.setPosition(720 - (column2.getWidth() / 2), 0);
        column2Collider = new Solid(0, 0, 240, 50, mainStage);
        column2Label = new Label("0", BaseGame.styleBig);
        column2Label.setPosition(720 - (column2Label.getWidth() / 2), column2.getY() + column2.getHeight() - 105);
        column2Label.setText("");
        column2Label.addAction(Actions.fadeOut(0));
        uiStage.addActor(column2Label);

        if (GameScore.nPlayers >= 3) {
            column3 = new Solid(-50, 0, 240, 347, mainStage);
            column3.loadTexture("src/apalacios/pfg/assets/world_elements/pilar.png");
            column3.setPosition(1200 - (column3.getWidth() / 2), 0);
            column3Collider = new Solid(0, 0, 240, 50, mainStage);
            column3Label = new Label("0", BaseGame.styleBig);
            column3Label.setPosition(1200 - (column3Label.getWidth() / 2), column3.getY() + column3.getHeight() - 105);
            column3Label.setText("");
            column3Label.addAction(Actions.fadeOut(0));
            uiStage.addActor(column3Label);
        }

        if (GameScore.nPlayers == 4) {
            column4 = new Solid(-50, 0, 240, 347, mainStage);
            column4.loadTexture("src/apalacios/pfg/assets/world_elements/pilar.png");
            column4.setPosition(1680 - (column4.getWidth() / 2), 0);
            column4Collider = new Solid(0, 0, 240, 50, mainStage);
            column4Label = new Label("0", BaseGame.styleBig);
            column4Label.setPosition(1680 - (column4Label.getWidth() / 2), column4.getY() + column4.getHeight() - 105);
            column4Label.setText("");
            column4Label.addAction(Actions.fadeOut(0));
            uiStage.addActor(column4Label);
        }

        // Leaderboard
        ArrayList<Integer> scores = new ArrayList<>();
        scores.add(GameScore.scoreP1);
        scores.add(GameScore.scoreP2);
        if (GameScore.nPlayers >= 3)
            scores.add(GameScore.scoreP3);
        if (GameScore.nPlayers == 4)
            scores.add(GameScore.scoreP4);

        Collections.sort(scores);

        if (GameScore.nPlayers == 4) {
            if (scores.get(0) == GameScore.scoreP1)
                this.player1.setPlace(4);
            if (scores.get(0) == GameScore.scoreP2)
                this.player2.setPlace(4);
            if (scores.get(0) == GameScore.scoreP3)
                this.player3.setPlace(4);
            if (scores.get(0) == GameScore.scoreP4)
                this.player4.setPlace(4);
        }

        if (GameScore.nPlayers >= 3) {
            if (scores.get(GameScore.nPlayers - 3) == GameScore.scoreP1)
                this.player1.setPlace(3);
            if (scores.get(GameScore.nPlayers - 3) == GameScore.scoreP2)
                this.player2.setPlace(3);
            if (scores.get(GameScore.nPlayers - 3) == GameScore.scoreP3)
                this.player3.setPlace(3);
        }

        if (scores.get(GameScore.nPlayers - 2) == GameScore.scoreP1)
            this.player1.setPlace(2);
        if (scores.get(GameScore.nPlayers - 2) == GameScore.scoreP2)
            this.player2.setPlace(2);
        if (scores.get(GameScore.nPlayers - 2) == GameScore.scoreP3)
            this.player3.setPlace(2);
        if (scores.get(GameScore.nPlayers - 2) == GameScore.scoreP4)
            this.player4.setPlace(2);

        if (scores.get(GameScore.nPlayers - 1) == GameScore.scoreP1)
            this.player1.setPlace(1);
        if (scores.get(GameScore.nPlayers - 1) == GameScore.scoreP2)
            this.player2.setPlace(1);
        if (scores.get(GameScore.nPlayers - 1) == GameScore.scoreP3)
            this.player3.setPlace(1);
        if (scores.get(GameScore.nPlayers - 1) == GameScore.scoreP4)
            this.player4.setPlace(1);

        column1Label.setText(this.player1.getPlace());
        column2Label.setText(this.player2.getPlace());
        if (GameScore.nPlayers >= 3)
            column3Label.setText(this.player3.getPlace());
        if (GameScore.nPlayers == 4)
            column4Label.setText(this.player4.getPlace());

        // Puntuaciones
        scoresShown = false;
        currentScore = GameScore.nPlayers;
        countDownTimer = System.currentTimeMillis();
        waitTime = 2000 + (2000 * GameScore.nPlayers);
        currentScreenScore = (4 - GameScore.nPlayers);

        // Retratos y nombres de los jugadores (pantalla)
        p1Color = new BaseActor(-50, -50, mainStage);
        p2Color = new BaseActor(-50, -50, mainStage);
        p3Color = new BaseActor(-50, -50, mainStage);
        p4Color = new BaseActor(-50, -50, mainStage);

        p1Skin = new BaseActor(-50, -50, mainStage);
        p2Skin = new BaseActor(-50, -50, mainStage);
        p3Skin = new BaseActor(-50, -50, mainStage);
        p4Skin = new BaseActor(-50, -50, mainStage);

        p1Label = new Label(player1.getPlayerName(), BaseGame.styleNormal);
        p2Label = new Label(player2.getPlayerName(), BaseGame.styleNormal);
        if (GameScore.nPlayers >= 3) {
            p3Label = new Label(player3.getPlayerName(), BaseGame.styleNormal);
            p3Label.setPosition(-100, -100);
            mainStage.addActor(p3Label);
        }
        if (GameScore.nPlayers == 4) {
            p4Label = new Label(player4.getPlayerName(), BaseGame.styleNormal);
            p4Label.setPosition(-100, -100);
            mainStage.addActor(p4Label);
        }

        p1Label.setPosition(-100, -100);
        p2Label.setPosition(-100, -100);

        mainStage.addActor(p1Label);
        mainStage.addActor(p2Label);

        p1ScoreLabel = new Label(GameScore.scoreP1 + "", BaseGame.styleNormal);
        p2ScoreLabel = new Label(GameScore.scoreP2 + "", BaseGame.styleNormal);
        p3ScoreLabel = new Label(GameScore.scoreP3 + "", BaseGame.styleNormal);
        p4ScoreLabel = new Label(GameScore.scoreP4 + "", BaseGame.styleNormal);
        p1ScoreLabel.setPosition(-100, -100);
        p2ScoreLabel.setPosition(-100, -100);
        p3ScoreLabel.setPosition(-100, -100);
        p4ScoreLabel.setPosition(-100, -100);
        mainStage.addActor(p1ScoreLabel);
        mainStage.addActor(p2ScoreLabel);

        if (GameScore.nPlayers >= 3)
            mainStage.addActor(p3ScoreLabel);
        if (GameScore.nPlayers == 4)
            mainStage.addActor(p4ScoreLabel);

        p1Color.loadTexture("src/apalacios/pfg/assets/characters/portraits/pj" + player1.getnColor() + ".png");
        p2Color.loadTexture("src/apalacios/pfg/assets/characters/portraits/pj" + player2.getnColor() + ".png");
        if (GameScore.nPlayers >= 3)
            p3Color.loadTexture("src/apalacios/pfg/assets/characters/portraits/pj" + player3.getnColor() + ".png");
        if (GameScore.nPlayers == 4)
            p4Color.loadTexture("src/apalacios/pfg/assets/characters/portraits/pj" + player4.getnColor() + ".png");

        p1Skin.loadTexture("src/apalacios/pfg/assets/characters/portraits/sk" + player1.getnSkin() + ".png");
        p2Skin.loadTexture("src/apalacios/pfg/assets/characters/portraits/sk" + player2.getnSkin() + ".png");
        if (GameScore.nPlayers >= 3)
            p3Skin.loadTexture("src/apalacios/pfg/assets/characters/portraits/sk" + player3.getnSkin() + ".png");
        if (GameScore.nPlayers == 4)
            p4Skin.loadTexture("src/apalacios/pfg/assets/characters/portraits/sk" + player4.getnSkin() + ".png");

        // Filtro para la pantalla
        BaseActor screenFilter = new BaseActor(0, 0, mainStage);
        screenFilter.loadTexture("src/apalacios/pfg/assets/background/victory_screen_filter.png");

        // Pantalla de carga
        BaseActor loadingScreen = new BaseActor(0, 0, uiStage);
        loadingScreen.loadTexture("src/apalacios/pfg/assets/background/loadingScreen.png");
        loadingScreen.addAction(Actions.fadeOut(0.5f));
        canContinue = false;

        // Música
        new Thread(() -> StartMenuScreen.titleMusic.setOnCompletionListener(music -> {
            if (StartMenuScreen.currentSong == 13)
                StartMenuScreen.currentSong = 1;
            else
                StartMenuScreen.currentSong++;

            String songName = "04 Shell Shock Shake";
            switch (StartMenuScreen.currentSong) {
                case 1:
                    songName = "01 Introjuice";
                    break;
                case 2:
                    songName = "02 Failien Funk";
                    break;
                case 3:
                    songName = "03 Stroll 'n Roll";
                    break;
                case 4:
                    songName = "04 Shell Shock Shake";
                    break;
                case 5:
                    songName = "05 I'm a Fighter";
                    break;
                case 6:
                    songName = "06 Going Down Tune";
                    break;
                case 7:
                    songName = "07 Cloud Crash";
                    break;
                case 8:
                    songName = "08 Filaments and Voids";
                    break;
                case 9:
                    songName = "09 Bonus Rage";
                    break;
                case 10:
                    songName = "10 It's not My Ship";
                    break;
                case 11:
                    songName = "11 Perihelium";
                    break;
                case 12:
                    songName = "12 Shingle Tingle";
                    break;
                case 13:
                    songName = "13 Just a Minuet";
                    break;
            }

            StartMenuScreen.titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                    ("src/apalacios/pfg/assets/music/" + songName + ".mp3"));
            StartMenuScreen.titleMusic.setLooping(false);
            StartMenuScreen.titleMusic.play();
            StartMenuScreen.titleMusic.setVolume(Config.musicVolume);
        })).start();

    }

    @Override
    public boolean buttonUp(Controller controller, int i) {
        if (canContinue) {
            for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                Player player = (Player) playerActor;
                if (player.getController() == controller && i == player.getControllerProfile().getButtonStart()) {
                    Util.lastScreen = LastScreen.ONEPLAYER;
                    PFGGame.setActiveScreen(new StartMenuScreen());
                }
            }
        }
        return false;
    }

    @Override
    public void update(float dt) {

        // Puntuaciones
        if (!scoresShown) {
            long currentPlayTime = System.currentTimeMillis();
            long time = countDownTimer - currentPlayTime;
            if (time < -(waitTime + 2000) && currentScore == 0) {
                canContinue = true;
                currentScore = -1;
            }else if (time < -waitTime && currentScore == 1) {
                if (player1.getPlace() == 1) {
                    column1Label.addAction(Actions.delay(1));
                    column1Label.addAction(Actions.after(Actions.fadeIn(0)));
                    setScoreOnScreen(player1);
                }
                if (player2.getPlace() == 1) {
                    column2Label.addAction(Actions.delay(1));
                    column2Label.addAction(Actions.after(Actions.fadeIn(0)));
                    setScoreOnScreen(player2);
                }
                if (GameScore.nPlayers >= 3) {
                    if (player3.getPlace() == 1) {
                        column3Label.addAction(Actions.delay(1));
                        column3Label.addAction(Actions.after(Actions.fadeIn(0)));
                        setScoreOnScreen(player3);
                    }
                }
                if (GameScore.nPlayers == 4) {
                    if (player4.getPlace() == 1) {
                        column4Label.addAction(Actions.delay(1));
                        column4Label.addAction(Actions.after(Actions.fadeIn(0)));
                        setScoreOnScreen(player4);
                    }
                }
                currentScore = 0;
            } else if (time < -(waitTime - 2000) && currentScore == 2) {
                generateCrate(2);
                if (player1.getPlace() == 2) {
                    column1Label.addAction(Actions.delay(1));
                    column1Label.addAction(Actions.after(Actions.fadeIn(0)));
                    column1.addAction(Actions.delay(1));
                    column1.addAction(Actions.after(Actions.moveBy(0, -75, 1f)));
                    setScoreOnScreen(player1);
                }
                if (player2.getPlace() == 2) {
                    column2Label.addAction(Actions.delay(1));
                    column2Label.addAction(Actions.after(Actions.fadeIn(0)));
                    column2.addAction(Actions.delay(1));
                    column2.addAction(Actions.after(Actions.moveBy(0, -75, 1f)));
                    setScoreOnScreen(player2);
                }
                if (GameScore.nPlayers >= 3) {
                    if (player3.getPlace() == 2) {
                        column3Label.addAction(Actions.delay(1));
                        column3Label.addAction(Actions.after(Actions.fadeIn(0)));
                        column3.addAction(Actions.delay(1));
                        column3.addAction(Actions.after(Actions.moveBy(0, -75, 1f)));
                        setScoreOnScreen(player3);
                    }
                }
                if (GameScore.nPlayers == 4) {
                    if (player4.getPlace() == 2) {
                        column4Label.addAction(Actions.delay(1));
                        column4Label.addAction(Actions.after(Actions.fadeIn(0)));
                        column4.addAction(Actions.delay(1));
                        column4.addAction(Actions.after(Actions.moveBy(0, -75, 1f)));
                        setScoreOnScreen(player4);
                    }
                }
                currentScore = 1;
            } else if (time < -(waitTime - 4000) && currentScore == 3) {
                generateCrate(3);
                if (player1.getPlace() == 3) {
                    column1Label.addAction(Actions.delay(1));
                    column1Label.addAction(Actions.after(Actions.fadeIn(0)));
                    column1.addAction(Actions.delay(1));
                    column1.addAction(Actions.after(Actions.moveBy(0, -125, 1f)));
                    setScoreOnScreen(player1);
                }
                if (player2.getPlace() == 3) {
                    column2Label.addAction(Actions.delay(1));
                    column2Label.addAction(Actions.after(Actions.fadeIn(0)));
                    column2.addAction(Actions.delay(1));
                    column2.addAction(Actions.after(Actions.moveBy(0, -125, 1f)));
                    setScoreOnScreen(player2);
                }
                if (GameScore.nPlayers >= 3) {
                    if (player3.getPlace() == 3) {
                        column3Label.addAction(Actions.delay(1));
                        column3Label.addAction(Actions.after(Actions.fadeIn(0)));
                        column3.addAction(Actions.delay(1));
                        column3.addAction(Actions.after(Actions.moveBy(0, -125, 1f)));
                        setScoreOnScreen(player3);
                    }
                }
                if (GameScore.nPlayers == 4) {
                    if (player4.getPlace() == 3) {
                        column4Label.addAction(Actions.delay(1));
                        column4Label.addAction(Actions.after(Actions.fadeIn(0)));
                        column4.addAction(Actions.delay(1));
                        column4.addAction(Actions.after(Actions.moveBy(0, -125, 1f)));
                        setScoreOnScreen(player4);
                    }
                }
                currentScore = 2;
            } else if (time < -(waitTime - 6000) && currentScore == 4) {
                generateCrate(4);
                if (player1.getPlace() == 4) {
                    column1Label.addAction(Actions.delay(1));
                    column1Label.addAction(Actions.after(Actions.fadeIn(0)));
                    column1.addAction(Actions.delay(1));
                    column1.addAction(Actions.after(Actions.moveBy(0, -200, 1f)));
                    setScoreOnScreen(player1);
                }
                if (player2.getPlace() == 4) {
                    column2Label.addAction(Actions.delay(1));
                    column2Label.addAction(Actions.after(Actions.fadeIn(0)));
                    column2.addAction(Actions.delay(1));
                    column2.addAction(Actions.after(Actions.moveBy(0, -200, 1)));
                    setScoreOnScreen(player2);
                }
                if (GameScore.nPlayers >= 3) {
                    if (player3.getPlace() == 4) {
                        column3Label.addAction(Actions.delay(1));
                        column3Label.addAction(Actions.after(Actions.fadeIn(0)));
                        column3.addAction(Actions.delay(1));
                        column3.addAction(Actions.after(Actions.moveBy(0, -200, 1)));
                        setScoreOnScreen(player3);
                    }
                }
                if (GameScore.nPlayers == 4) {
                    if (player4.getPlace() == 4) {
                        column4Label.addAction(Actions.delay(1));
                        column4Label.addAction(Actions.after(Actions.fadeIn(0)));
                        column4.addAction(Actions.delay(1));
                        column4.addAction(Actions.after(Actions.moveBy(0, -200, 0.8f)));
                        setScoreOnScreen(player4);
                    }
                }
                currentScore = 3;
            }
        }

        // Ajustar posición de los collider y el texto de las columnas
        column1Collider.setPosition(column1.getX(), column1.getY() + column1.getHeight() - 30);
        column1Label.setPosition(240 - (column1Label.getWidth() / 2), column1.getY() + column1.getHeight() - 105);
        column2Collider.setPosition(column2.getX(), column2.getY() + column2.getHeight() - 30);
        column2Label.setPosition(720 - (column2Label.getWidth() / 2), column2.getY() + column2.getHeight() - 105);
        if (GameScore.nPlayers >= 3) {
            column3Collider.setPosition(column3.getX(), column3.getY() + column3.getHeight() - 30);
            column3Label.setPosition(1200 - (column3Label.getWidth() / 2), column3.getY() + column3.getHeight() - 105);
        }

        if (GameScore.nPlayers == 4) {
            column4Collider.setPosition(column4.getX(), column4.getY() + column4.getHeight() - 30);
            column4Label.setPosition(1680 - (column4Label.getWidth() / 2), column4.getY() + column4.getHeight() - 105);
        }

        for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
            Player player = (Player) playerActor;
            player.setOnGround(false);
            player.setOnPlatform(false);
        }

        // COLISION JUGADORES -> SUELO
        for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
            for (BaseActor mobileActor : BaseActor.getList(mainStage, Player.class.getName())) {
                Player playerActor = (Player) mobileActor;
                if (playerActor.isAlive()) {
                    if (playerActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                        if (playerActor.getFeetSensorL().overlaps(solidActor) &&
                                playerActor.getFeetSensorR().overlaps(solidActor)) {
                            playerActor.setPosition(playerActor.getX(), solidActor.getY() + solidActor.getHeight() + 2);
                        }
                    }

                    if (playerActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                        if (playerActor.getFeetSensorL().overlaps(solidActor) ||
                                playerActor.getFeetSensorR().overlaps(solidActor)) {
                            playerActor.setGravityEnabled(false);
                            playerActor.setOnGround(true);
                        }
                    } else {
                        playerActor.preventOverlap(solidActor);
                    }
                }
            }
        }

        // COLISION JUGADOR -> CAJA
        for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
            Player player = (Player) playerActor;

            if (player.isAlive()) {
                for (BaseActor crateActor : BaseActor.getList(mainStage, Crate.class.getName())) {
                    Crate crate = (Crate) crateActor;

                    if (crate.getFeetSensor().overlaps(player) && crate.getVelocityVector().y < 0) {
                        crate.destroy();
                        player.die(DeadType.CRUSHED);
                        break;
                    }
                }
            }
        }

        // ACTIVAR GRAVEDAD
        for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
            Player player = (Player) playerActor;
            if (!player.isOnPlatform() && !player.isOnGround())
                player.setGravityEnabled(true);
        }
    }

    private void setScoreOnScreen(Player player) { // 3 -> 905, 2  -> 829, 1 -> 753, 0 -> 677
        switch (player.getnPlayer()) {
            case 1: {
                p1Color.setPosition(658, 677 + (currentScreenScore * 76));
                p1Skin.centerAtActor(p1Color);
                p1Label.setPosition(752, p1Color.getY() + 33 - (p1Label.getHeight() / 2));
                p1ScoreLabel.setPosition(1240 - (p1ScoreLabel.getWidth() / 2),
                        p1Color.getY() + 33 - (p1ScoreLabel.getHeight() / 2));
                break;
            }
            case 2: {
                p2Color.setPosition(658, 677 + (currentScreenScore * 76));
                p2Skin.centerAtActor(p2Color);
                p2Label.setPosition(752, p2Color.getY() + 33 - (p2Label.getHeight() / 2));
                p2ScoreLabel.setPosition(1240 - (p2ScoreLabel.getWidth() / 2),
                        p2Color.getY() + 33 - (p2ScoreLabel.getHeight() / 2));
                break;
            }
            case 3: {
                p3Color.setPosition(658, 677 + (currentScreenScore * 76));
                p3Skin.centerAtActor(p3Color);
                p3Label.setPosition(752, p3Color.getY() + 33 - (p3Label.getHeight() / 2));
                p3ScoreLabel.setPosition(1240 - (p3ScoreLabel.getWidth() / 2),
                        p3Color.getY() + 33 - (p3ScoreLabel.getHeight() / 2));
                break;
            }
            case 4: {
                p4Color.setPosition(658, 677 + (currentScreenScore * 76));
                p4Skin.centerAtActor(p4Color);
                p4Label.setPosition(752, p4Color.getY() + 33 - (p4Label.getHeight() / 2));
                p4ScoreLabel.setPosition(1240 - (p4ScoreLabel.getWidth() / 2),
                        p4Color.getY() + 33 - (p4ScoreLabel.getHeight() / 2));
                break;
            }
        }
        currentScreenScore++;
    }

    private void generateCrate(int place) {
        for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
            Player player = (Player) playerActor;
            if (player.getPlace() == place) {
                Crate crate = new Crate(-100, -100, 72, 72, mainStage);
                crate.setPosition(player.getX() + (player.getWidth() / 2) - (crate.getWidth() / 2), 1090);
                crate.setBigCrate(true);
            }
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
        mainStage.getViewport().update(arg0, arg1, true);
    }
}
