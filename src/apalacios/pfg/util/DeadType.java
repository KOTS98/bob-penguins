package apalacios.pfg.util;

public enum DeadType {
    CRUSHED, SHOT, VOID
}
