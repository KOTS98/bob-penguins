package apalacios.pfg.screens;

import apalacios.pfg.characters.BaseActor;
import apalacios.pfg.characters.PenguinMenu;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.game.PFGGame;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.LastScreen;
import apalacios.pfg.util.Profile;
import apalacios.pfg.util.Util;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class StartMenuScreen extends BaseGamepadScreen {

    private long startInteractionTime;

    private PenguinMenu pm1;
    private PenguinMenu pm2;
    private BaseActor platform;

    private int selectedOption;
    private int maxMenuOption;
    private enum Menu {
        MAIN, CONF, MUSIC, SOUND
    }
    private Menu currentMenu;

    // Menú principal
    private Label unJugadorLabel;
    private Label multiJugadorLabel;
    private Label configuracionLabel;
    private Label salirLabel;

    // Menú configuración
    private Label musicaLabel;
    private Slider musicSlider;
    private Label efectosSonidoLabel;
    private Label modoVisualizacionLabel;
    private Slider soundSlider;
    private Label perfilesLabel;

    // Perfiles
    private ArrayList<Profile> profiles;
    private Label minimoUnPerfilLabel;
    private float timeAlive;
    private boolean labelMinProfileAlive;

    // Música y sonido
    static Music titleMusic;
    static int currentSong;
    private Sound navigateSound;
    private Sound acceptSound;

    @Override
    public void initialize() {
        startInteractionTime = System.currentTimeMillis();

        Config.loadConfig();

        if (Config.fullScreen)
            Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
        else
            Gdx.graphics.setWindowedMode(Config.screenWidth, Config.screenHeight);

        BaseActor background = new BaseActor(0, 0, mainStage);

        background.loadTexture("src/apalacios/pfg/assets/background/start_menu_screen.png");
        background.setSize(1920, 1080);

        BaseActor.setWorldBounds(background);

        platform = new BaseActor(0, 640, mainStage);
        platform.setSize(1920, 90);
        platform.setBoundaryRectangle();

        pm1 = new PenguinMenu(500, 730, mainStage);
        pm2 = new PenguinMenu(200, 730, mainStage);

        // Interfaz
        selectedOption = 1;
        currentMenu = Menu.MAIN;

        // Menú principal
        unJugadorLabel = new Label("UN JUGADOR", BaseGame.styleNormal);
        unJugadorLabel.setColor(Color.RED);
        unJugadorLabel.setPosition(850, -50);
        uiStage.addActor(unJugadorLabel);

        multiJugadorLabel = new Label("MULTIJUGADOR", BaseGame.styleNormal);
        multiJugadorLabel.setPosition(830, -50);
        uiStage.addActor(multiJugadorLabel);

        configuracionLabel = new Label("CONFIGURACION", BaseGame.styleNormal);
        configuracionLabel.setPosition(820, -50);
        uiStage.addActor(configuracionLabel);

        salirLabel = new Label("SALIR DEL JUEGO", BaseGame.styleNormal);
        salirLabel.setPosition(810, -50);
        uiStage.addActor(salirLabel);

        // Menú configuración
        musicaLabel = new Label("MUSICA", BaseGame.styleNormal);
        musicaLabel.setPosition(895, -50);
        uiStage.addActor(musicaLabel);
        musicaLabel.setColor(Color.RED);

        musicSlider = new Slider(0, 100, 5, false, BaseGame.uiSkin);
        musicSlider.setPosition(760, -60);
        musicSlider.setValue(Config.musicVolume * 100);
        musicSlider.setSize(400, musicSlider.getHeight());
        uiStage.addActor(musicSlider);

        efectosSonidoLabel = new Label("EFFECTOS DE SONIDO", BaseGame.styleNormal);
        efectosSonidoLabel.setPosition(778, -50);
        uiStage.addActor(efectosSonidoLabel);

        soundSlider = new Slider(0, 100, 5, false, BaseGame.uiSkin);
        soundSlider.setPosition(760, -60);
        soundSlider.setValue(Config.soundVolume * 100);
        soundSlider.setSize(400, soundSlider.getHeight());
        uiStage.addActor(soundSlider);

        // Perfiles
        profiles = Util.loadProfiles();
        minimoUnPerfilLabel = new Label("NECESITAS UN PERFIL PARA JUGAR!", BaseGame.styleNormal);
        minimoUnPerfilLabel.setColor(Color.RED);
        minimoUnPerfilLabel.setPosition(960 - (minimoUnPerfilLabel.getWidth() / 2), 570);
        labelMinProfileAlive = false;
        timeAlive = 0;

        if (Config.fullScreen)
            modoVisualizacionLabel = new Label("MODO VENTANA", BaseGame.styleNormal);
        else
            modoVisualizacionLabel = new Label("PANTALLA COMPLETA", BaseGame.styleNormal);
        modoVisualizacionLabel.setPosition(960 - (modoVisualizacionLabel.getWidth() / 2), -50);
        uiStage.addActor(modoVisualizacionLabel);

        perfilesLabel = new Label("PERFILES", BaseGame.styleNormal);
        perfilesLabel.setPosition(870, -50);
        uiStage.addActor(perfilesLabel);

        // Música y sonidos
        navigateSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/menu_navigate.ogg"));
        acceptSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/menu_accept.ogg"));

        if (Util.lastScreen == null) {
            maxMenuOption = 4;
            if (titleMusic == null)
                titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                        ("src/apalacios/pfg/assets/music/04 Shell Shock Shake.mp3"));
            titleMusic.setLooping(false);
            titleMusic.play();
            titleMusic.setVolume(Config.musicVolume);
            animateMainMenu(true, true);
        } else if (Util.lastScreen == LastScreen.PROFILES) {
            currentMenu = Menu.CONF;
            maxMenuOption = 4;
            selectedOption = 4;
            animateConfMenu(true);
            updateMenu(0);
        } else if (Util.lastScreen == LastScreen.ONEPLAYER) {
            maxMenuOption = 4;
            animateMainMenu(false, true);
        }

        currentSong = 4;

        new Thread(() -> titleMusic.setOnCompletionListener(music -> {
            if (currentSong == 13)
                currentSong = 1;
            else
                currentSong++;

            String songName = "04 Shell Shock Shake";
            switch (currentSong) {
                case 1:
                    songName = "01 Introjuice";
                    break;
                case 2:
                    songName = "02 Failien Funk";
                    break;
                case 3:
                    songName = "03 Stroll 'n Roll";
                    break;
                case 4:
                    songName = "04 Shell Shock Shake";
                    break;
                case 5:
                    songName = "05 I'm a Fighter";
                    break;
                case 6:
                    songName = "06 Going Down Tune";
                    break;
                case 7:
                    songName = "07 Cloud Crash";
                    break;
                case 8:
                    songName = "08 Filaments and Voids";
                    break;
                case 9:
                    songName = "09 Bonus Rage";
                    break;
                case 10:
                    songName = "10 It's not My Ship";
                    break;
                case 11:
                    songName = "11 Perihelium";
                    break;
                case 12:
                    songName = "12 Shingle Tingle";
                    break;
                case 13:
                    songName = "13 Just a Minuet";
                    break;
            }

            titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                    ("src/apalacios/pfg/assets/music/" + songName + ".mp3"));
            titleMusic.setLooping(false);
            titleMusic.play();
            titleMusic.setVolume(Config.musicVolume);
        })).start();
    }

    private void animateMainMenu(boolean delay, boolean intro) {
        startInteractionTime = System.currentTimeMillis();
        if (intro) {
            if (delay) {
                unJugadorLabel.addAction(Actions.delay(1));
                multiJugadorLabel.addAction(Actions.delay(1));
                configuracionLabel.addAction(Actions.delay(1));
                salirLabel.addAction(Actions.delay(1));
            }
            unJugadorLabel.addAction(Actions.after(Actions.moveTo(850, 500, 0.2f)));
            multiJugadorLabel.addAction(Actions.after(Actions.moveTo(830, 420, 0.3f)));
            configuracionLabel.addAction(Actions.after(Actions.moveTo(820, 340, 0.4f)));
            salirLabel.addAction(Actions.after(Actions.moveTo(810, 260, 0.5f)));
        } else {
            unJugadorLabel.addAction(Actions.moveTo(-300, 500, 0.4f));
            multiJugadorLabel.addAction(Actions.moveTo(1930, 420, 0.3f));
            configuracionLabel.addAction(Actions.moveTo(-300, 340, 0.2f));
            salirLabel.addAction(Actions.moveTo(1930, 260, 0.1f));

            // Reposicionar abajo
            unJugadorLabel.addAction(Actions.after(Actions.moveTo(850, -50)));
            multiJugadorLabel.addAction(Actions.after(Actions.moveTo(830, -50)));
            configuracionLabel.addAction(Actions.after(Actions.moveTo(820, -50)));
            salirLabel.addAction(Actions.after(Actions.moveTo(810, -50)));
        }
    }

    private void animateConfMenu(boolean intro) {
        startInteractionTime = System.currentTimeMillis();
        if (intro) {
            musicaLabel.addAction(Actions.moveTo(895, 500, 0.2f));
            musicSlider.addAction(Actions.moveTo(760, 440, 0.3f));
            efectosSonidoLabel.addAction(Actions.moveTo(778, 380, 0.4f));
            soundSlider.addAction(Actions.moveTo(760, 320, 0.5f));
            modoVisualizacionLabel.addAction(Actions.moveTo(960 - (modoVisualizacionLabel.getWidth() / 2), 260, 0.6f));
            perfilesLabel.addAction(Actions.moveTo(870, 200, 0.7f));
        } else {
            musicaLabel.addAction(Actions.moveTo(-300, 500, 0.7f));
            musicSlider.addAction(Actions.moveTo(1930, 440, 0.6f));
            efectosSonidoLabel.addAction(Actions.moveTo(-370, 380, 0.5f));
            soundSlider.addAction(Actions.moveTo(1930, 320, 0.4f));
            modoVisualizacionLabel.addAction(Actions.moveTo(-300, 260, 0.3f));
            perfilesLabel.addAction(Actions.moveTo(1930, 200, 0.2f));

            // Reposicionar abajo
            musicaLabel.addAction(Actions.after(Actions.moveTo(895, -50)));
            musicSlider.addAction(Actions.after(Actions.moveTo(760, -60)));
            efectosSonidoLabel.addAction(Actions.after(Actions.moveTo(778, -50)));
            soundSlider.addAction(Actions.after(Actions.moveTo(760, -60)));
            modoVisualizacionLabel.addAction(Actions.after(Actions.moveTo(960 - (modoVisualizacionLabel.getWidth() / 2), -50)));
            perfilesLabel.addAction(Actions.after(Actions.moveTo(870, -50)));
        }
    }

    @Override
    public boolean povMoved(Controller controller, int i, PovDirection povDirection) {
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < -500) {
            if (currentMenu == Menu.SOUND || currentMenu == Menu.MUSIC) {
                if (povDirection == PovDirection.east) {
                    updateMenu(1);
                } else if (povDirection == PovDirection.west) {
                    updateMenu(-1);
                }
            } else {
                if (povDirection == PovDirection.north) {
                    updateMenu(-1);
                } else if (povDirection == PovDirection.south) {
                    updateMenu(1);
                }
            }
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int i, float v) {
        // i Representa la palanca del mando y el eje en el que se ha movido
        // Palanca derecha: Eje vertical -> 0 Eje horizontal -> 1
        // Palanca izquierda: Ejeve vertical -> 2 Eje horizontal -> 3
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < -500) {
            if (currentMenu == Menu.SOUND || currentMenu == Menu.MUSIC) {
                if (i == 3 && (v == 1 || v == -1)) {
                    updateMenu((int) v);
                }
            } else {
                if (i == 2 && (v == 1 || v == -1)) {
                    updateMenu((int) v);
                }
            }
        }
        return false;
    }

    private void goBack() {
        switch (currentMenu) {
            case CONF:
                animateConfMenu(false);
                animateMainMenu(false, true);
                currentMenu = Menu.MAIN;
                selectedOption = 3;
                maxMenuOption = 4;
                updateMenu(0);
                break;
            case MUSIC:
                musicSlider.clearActions();
                musicSlider.setColor(Color.WHITE);
                currentMenu = Menu.CONF;
                Config.saveConfig();
                break;
            case SOUND:
                soundSlider.clearActions();
                soundSlider.setColor(Color.WHITE);
                currentMenu = Menu.CONF;
                Config.saveConfig();
                break;
        }
    }

    @Override
    public boolean buttonUp(Controller controller, int i) {
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < -500) {
            if (i == 0)
                optionSelected();
            else if (i == 1)
                goBack();
        }
        return false;
    }

    private void updateMenu(int value) {
        navigateSound.play(Config.soundVolume);
        if (value == 1 || value == -1) {
            if (currentMenu != Menu.SOUND && currentMenu != Menu.MUSIC) {
                if (value == 1) {
                    if (selectedOption + 1 <= maxMenuOption) {
                        selectedOption++;
                    } else {
                        selectedOption = 1;
                    }
                } else {
                    if (selectedOption - 1 >= 1) {
                        selectedOption--;
                    } else {
                        selectedOption = maxMenuOption;
                    }
                }
            }
        }
        switch (currentMenu) {
            case MAIN:
                unJugadorLabel.setColor(Color.WHITE);
                multiJugadorLabel.setColor(Color.WHITE);
                configuracionLabel.setColor(Color.WHITE);
                salirLabel.setColor(Color.WHITE);

                switch (selectedOption) {
                    case 1:
                        unJugadorLabel.setColor(Color.RED);
                        break;
                    case 2:
                        multiJugadorLabel.setColor(Color.RED);
                        break;
                    case 3:
                        configuracionLabel.setColor(Color.RED);
                        break;
                    case 4:
                        salirLabel.setColor(Color.RED);
                        break;
                }
                break;
            case CONF:
                musicaLabel.setColor(Color.WHITE);
                efectosSonidoLabel.setColor(Color.WHITE);
                modoVisualizacionLabel.setColor(Color.WHITE);
                perfilesLabel.setColor(Color.WHITE);

                switch (selectedOption) {
                    case 1:
                        musicaLabel.setColor(Color.RED);
                        break;
                    case 2:
                        efectosSonidoLabel.setColor(Color.RED);
                        break;
                    case 3:
                        modoVisualizacionLabel.setColor(Color.RED);
                        break;
                    case 4:
                        perfilesLabel.setColor(Color.RED);
                        break;
                }
                break;
            case MUSIC:
                if (value == 1) {
                    if (musicSlider.getValue() + 5 <= musicSlider.getMaxValue())
                        musicSlider.setValue(musicSlider.getValue() + 5);
                    Config.musicVolume = musicSlider.getValue() / 100;
                    titleMusic.setVolume(Config.musicVolume);
                } else if (value == -1) {
                    if (musicSlider.getValue() - 5 >= musicSlider.getMinValue())
                        musicSlider.setValue(musicSlider.getValue() - 5);
                    Config.musicVolume = musicSlider.getValue() / 100;
                    titleMusic.setVolume(Config.musicVolume);
                }
                break;
            case SOUND:
                if (value == 1) {
                    if (soundSlider.getValue() + 5 <= soundSlider.getMaxValue())
                        soundSlider.setValue(soundSlider.getValue() + 5);
                    Config.soundVolume = soundSlider.getValue() / 100;
                } else if (value == -1) {
                    if (soundSlider.getValue() - 5 >= soundSlider.getMinValue())
                        soundSlider.setValue(soundSlider.getValue() - 5);
                    Config.soundVolume = soundSlider.getValue() / 100;
                }
                break;
        }
    }

    private void optionSelected() {
        switch (currentMenu) {
            case MAIN:
                acceptSound.play(Config.soundVolume);
                switch (selectedOption) {
                    case 1:
                        if (profiles.size() > 0)
                            PFGGame.setActiveScreen(new OnePlayerScreen());
                        else {
                            if (!labelMinProfileAlive) {
                                labelMinProfileAlive = true;
                                uiStage.addActor(minimoUnPerfilLabel);
                                minimoUnPerfilLabel.addAction(Actions.moveBy(0, 50, 3));
                                minimoUnPerfilLabel.addAction(Actions.delay(1));
                                minimoUnPerfilLabel.addAction(Actions.after(Actions.fadeOut(1)));
                                minimoUnPerfilLabel.addAction(Actions.after(Actions.removeActor()));
                                minimoUnPerfilLabel.addAction(Actions.after(Actions.fadeIn(0)));
                                minimoUnPerfilLabel.addAction(Actions.after(Actions.moveBy(0, -50)));
                            }
                        }
                        break;
                    case 2:
                        if (profiles.size() > 0)
                            PFGGame.setActiveScreen(new MultiplayerScreen());
                        else {
                            if (!labelMinProfileAlive) {
                                labelMinProfileAlive = true;
                                uiStage.addActor(minimoUnPerfilLabel);
                                minimoUnPerfilLabel.addAction(Actions.moveBy(0, 50, 3));
                                minimoUnPerfilLabel.addAction(Actions.delay(1));
                                minimoUnPerfilLabel.addAction(Actions.after(Actions.fadeOut(1)));
                                minimoUnPerfilLabel.addAction(Actions.after(Actions.removeActor()));
                                minimoUnPerfilLabel.addAction(Actions.after(Actions.fadeIn(0)));
                                minimoUnPerfilLabel.addAction(Actions.after(Actions.moveBy(0, -50)));
                            }
                        }
                        break;
                    case 3:
                        currentMenu = Menu.CONF;
                        selectedOption = 1;
                        maxMenuOption = 4;
                        updateMenu(0);
                        animateMainMenu(false, false);
                        animateConfMenu(true);
                        break;
                    case 4:
                        Gdx.app.exit();
                        System.exit(0);
                        break;
                }
                break;
            case CONF:
                switch (selectedOption) {
                    case 1:
                        acceptSound.play(Config.soundVolume);
                        musicSlider.addAction(Actions.forever(Actions.sequence(Actions.color(Color.BLUE, 0.1f),
                                Actions.color(Color.WHITE, 0.3f))));
                        currentMenu = Menu.MUSIC;
                        break;
                    case 2:
                        acceptSound.play(Config.soundVolume);
                        soundSlider.addAction(Actions.forever(Actions.sequence(Actions.color(Color.BLUE, 0.1f),
                                Actions.color(Color.WHITE, 0.3f))));
                        currentMenu = Menu.SOUND;
                        break;
                    case 3:
                        acceptSound.play(Config.soundVolume);
                        modoVisualizacionLabel.remove();
                        if (modoVisualizacionLabel.getText().toString().equalsIgnoreCase("PANTALLA COMPLETA")) {
                            Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
                            modoVisualizacionLabel = new Label("MODO VENTANA", BaseGame.styleNormal);
                            Config.fullScreen = true;
                        } else {
                            Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
                            Gdx.graphics.setWindowedMode((int) dimension.getWidth(), (int) dimension.getHeight());
                            modoVisualizacionLabel = new Label("PANTALLA COMPLETA", BaseGame.styleNormal);
                            Config.fullScreen = false;
                            Config.screenWidth = (int) dimension.getWidth();
                            Config.screenHeight = (int) dimension.getHeight();
                        }
                        Config.saveConfig();
                        uiStage.addActor(modoVisualizacionLabel);
                        modoVisualizacionLabel.setPosition(960 -(modoVisualizacionLabel.getWidth() / 2), 260);
                        modoVisualizacionLabel.setColor(Color.RED);
                        break;
                    case 4:
                        acceptSound.play(Config.soundVolume);
                        PFGGame.setActiveScreen(new ProfilesScreen());
                        break;
                }
                break;
        }
    }

    @Override
    public void update(float dt) {
        if (labelMinProfileAlive) {
            timeAlive += dt;

            if (timeAlive >= 4.5f) {
                timeAlive = 0;
                labelMinProfileAlive = false;
            }
        }



        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < -500) {
            if (currentMenu == Menu.SOUND || currentMenu == Menu.MUSIC) {
                if (Gdx.input.isKeyJustPressed(Keys.LEFT)) {
                    updateMenu(-1);
                } else if (Gdx.input.isKeyJustPressed(Keys.RIGHT)) {
                    updateMenu(1);
                }
            } else {
                if (Gdx.input.isKeyJustPressed(Keys.UP)) {
                    updateMenu(-1);
                } else if (Gdx.input.isKeyJustPressed(Keys.DOWN)) {
                    updateMenu(1);
                }
            }

            if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
                optionSelected();
            }
            if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
                goBack();
            }
        }

        pm1.preventOverlap(platform);
        pm1.getSkin().preventOverlap(platform);
        pm2.preventOverlap(platform);
        pm2.getSkin().preventOverlap(platform);

        if (pm1.collisionBox.overlaps(pm2.collisionBox)) {
            if (new Random().nextInt(100) + 1 <= 50) {
                pm1.kick(pm2);
                pm2.preventOverlap(pm1);
            } else {
                pm2.kick(pm1);
                pm1.preventOverlap(pm2);
            }
        }

        if (pm1.getY() <= 700 || pm2.getY() <= 700) {
            pm1.setPosition(pm1.getX(), 730);
            pm2.setPosition(pm2.getX(), 730);
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
        mainStage.getViewport().update(arg0, arg1, true);
    }
}
