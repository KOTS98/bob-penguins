package apalacios.pfg.characters;

import apalacios.pfg.util.Config;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Trampoline extends BaseActor {
    private boolean jumping;
    private boolean objectJumped;
    private long jumpStartTime;

    private Sound jumpSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/trampolin.ogg"));


    private Mobile currentMobile;
    /**
     * Constructor de la calse, inicializar los parámetros de la misma
     *
     * @param posX  Representa su posición respecto a la pantalla en el eje horizontal
     * @param posY  Representa su posición respecto a la pantalla en el eje vertical
     * @param stage El actor (independientemente de lo que sea), sera añadido al stage,
     */
    public Trampoline(float posX, float posY, Stage stage) {
        super(posX, posY, stage);
        jumping = false;
        objectJumped = false;
        jumpStartTime = System.currentTimeMillis();
        setAnimation(loadTexture("src/apalacios/pfg/assets/world_elements/trampoline_stand.png"));
    }

    public void jump(Mobile mobile) {
        currentMobile = mobile;
        if (!jumping) {
            jumping = true;
            objectJumped = false;
            jumpStartTime = System.currentTimeMillis();
            setAnimation(loadAnimationFromSheet("src/apalacios/pfg/assets/world_elements/trampline_jump.png",
                    1, 8, 0.04f, true));
            jumpSound.play(Config.soundVolume);
        }
    }

    @Override
    public void act(float dt) {
        super.act(dt);

        if (jumping) {
            long currentJumpTime = System.currentTimeMillis();
            if (jumpStartTime - currentJumpTime < -100 && !objectJumped) {
                objectJumped = true;
                currentMobile.trampolineJump();
            }
            if (jumpStartTime - currentJumpTime < -300) {
                setAnimation(loadTexture("src/apalacios/pfg/assets/world_elements/trampoline_stand.png"));
                jumping = false;
            }
        }
    }
}