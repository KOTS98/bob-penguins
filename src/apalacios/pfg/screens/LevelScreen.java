package apalacios.pfg.screens;

import apalacios.pfg.characters.*;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.game.PFGGame;
import apalacios.pfg.util.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.ArrayList;
import java.util.Random;

public class LevelScreen extends BaseGamepadScreen {

    // Jugadores
    private Player jugador1;
    private Player jugador2;
    private Player jugador3;
    private Player jugador4;
    private ArrayList<Integer> puntosInicioEnUso;

    // Pantalla de carga
    private BaseActor pantallaCarga;
    private BaseActor cuentaAtras;
    private long tiempoCuentaAtras;
    private boolean juegoListo;
    private int numeroCuentaAtras = 3;
    private boolean rondaAcabando;
    private int nNivel;

    private long tiempoFinDeRonda;
    private boolean finDeRonda;

    // Sonidos
    private Sound sonidoCuentaAtras;
    private Sound sonidoInicio;
    private Sound puntoObtenido;

    // Evitar bloqueos de la I.A.
    private long inicioSoloIA;
    private boolean inicioAntiBloqueo;
    private int tiempoEsperaMuerte;

    LevelScreen(Player jugador1, Player jugador2, Player jugador3, Player jugador4, int nNivel) {
        cargarFondo(nNivel);
        TiledMapActor tma = new TiledMapActor("src/apalacios/pfg/assets/map/map" + nNivel + ".tmx", mainStage);
        GameScore.lastLevel = nNivel;
        this.nNivel = nNivel;

        // Cargar mapa
        generarTerreno(tma);
        generarPlataformas(tma);
        generarCajas(tma);
        generarTrampolines(tma);
        generarArmas(tma);

        posicionarJugadores(jugador1, jugador2, jugador3, jugador4, tma);

        // Carga inicial
        cuentaAtras = new BaseActor(960, 540, uiStage);
        cuentaAtras.loadTexture("src/apalacios/pfg/assets/count_down/numero3.png");
        cuentaAtras.setPosition(960 - (cuentaAtras.getWidth() / 2), 540 - (cuentaAtras.getHeight() / 2));
        cuentaAtras.addAction(Actions.fadeOut(0));
        pantallaCarga = new BaseActor(0, 0, uiStage);
        pantallaCarga.loadTexture("src/apalacios/pfg/assets/background/loadingScreen.png");
        pantallaCarga.addAction(Actions.fadeOut(0.5f));
        tiempoCuentaAtras = System.currentTimeMillis();
        juegoListo = false;
        finDeRonda = false;

        // Sonido
        sonidoCuentaAtras = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/321.ogg"));
        sonidoInicio = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/ya.ogg"));
        puntoObtenido = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/puntoObtenido.ogg"));

        reproducirMusica();

        // Evitar bloqueo de la I.A.
        inicioAntiBloqueo = false;
        tiempoEsperaMuerte = 10000;
    }

    private void reproducirMusica() {
        new Thread(() -> StartMenuScreen.titleMusic.setOnCompletionListener(music -> {
            if (StartMenuScreen.currentSong == 13)
                StartMenuScreen.currentSong = 1;
            else
                StartMenuScreen.currentSong++;

            String songName = "04 Shell Shock Shake";
            switch (StartMenuScreen.currentSong) {
                case 1:
                    songName = "01 Introjuice";
                    break;
                case 2:
                    songName = "02 Failien Funk";
                    break;
                case 3:
                    songName = "03 Stroll 'n Roll";
                    break;
                case 4:
                    songName = "04 Shell Shock Shake";
                    break;
                case 5:
                    songName = "05 I'm a Fighter";
                    break;
                case 6:
                    songName = "06 Going Down Tune";
                    break;
                case 7:
                    songName = "07 Cloud Crash";
                    break;
                case 8:
                    songName = "08 Filaments and Voids";
                    break;
                case 9:
                    songName = "09 Bonus Rage";
                    break;
                case 10:
                    songName = "10 It's not My Ship";
                    break;
                case 11:
                    songName = "11 Perihelium";
                    break;
                case 12:
                    songName = "12 Shingle Tingle";
                    break;
                case 13:
                    songName = "13 Just a Minuet";
                    break;
            }

            StartMenuScreen.titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                    ("src/apalacios/pfg/assets/music/" + songName + ".mp3"));
            StartMenuScreen.titleMusic.setLooping(false);
            StartMenuScreen.titleMusic.play();
            StartMenuScreen.titleMusic.setVolume(Config.musicVolume);
        })).start();
    }

    private void cargarFondo(int nLevel) {
        // Colina -> 1, 5, 8, 9, 11, 14, 15 | nieve -> 2, 3, 6, 7, 12, 13 | edificios -> 4, 10
        BaseActor background = new BaseActor(0, 0, mainStage);
        if (nLevel == 1 || nLevel == 5 || nLevel == 8 || nLevel == 9 || nLevel == 11 || nLevel == 14 || nLevel == 15)
            background.loadTexture("src/apalacios/pfg/assets/background/colina.png");
        if (nLevel == 2 || nLevel == 3 || nLevel == 6 || nLevel == 7 || nLevel == 12  || nLevel == 13)
            background.loadTexture("src/apalacios/pfg/assets/background/nieve.png");
        if (nLevel == 4 || nLevel == 10)
            background.loadTexture("src/apalacios/pfg/assets/background/edificios.png");
    }

    private void posicionarJugadores(Player player1, Player player2, Player player3, Player player4, TiledMapActor tma) {
        puntosInicioEnUso = new ArrayList<>();

        MapObject startPoint1 = tma.getRectangleList("Start").get(obtenerPuntoInicio());
        MapProperties startP1Props = startPoint1.getProperties();
        this.jugador1 = new Player((float) startP1Props.get("x"), (float) startP1Props.get("y"), mainStage,
                player1.getnColor(), player1.getnSkin(), player1.isCPU());
        this.jugador1.setnPlayer(1);
        this.jugador1.setPlayerName(player1.getPlayerName(), uiStage);
        this.jugador1.setController(player1.getController());
        this.jugador1.setControllerProfile(player1.getControllerProfile());


        MapObject startPoint2 = tma.getRectangleList("Start").get(obtenerPuntoInicio());
        MapProperties startP2Props = startPoint2.getProperties();
        this.jugador2 = new Player((float) startP2Props.get("x"), (float) startP2Props.get("y"), mainStage,
                player2.getnColor(), player2.getnSkin(), player2.isCPU());
        this.jugador2.setnPlayer(2);
        this.jugador2.setPlayerName(player2.getPlayerName(), uiStage);
        if (!player2.isCPU()) {
            this.jugador2.setController(player2.getController());
            this.jugador2.setControllerProfile(player2.getControllerProfile());
        }

        if (player3 != null) {
            MapObject startPoint3 = tma.getRectangleList("Start").get(obtenerPuntoInicio());
            MapProperties startP3Props = startPoint3.getProperties();
            this.jugador3 = new Player((float) startP3Props.get("x"), (float) startP3Props.get("y"), mainStage,
                    player3.getnColor(), player3.getnSkin(), player3.isCPU());
            this.jugador3.setnPlayer(3);
            this.jugador3.setPlayerName(player3.getPlayerName(), uiStage);
            if (!player3.isCPU()) {
                this.jugador3.setController(player3.getController());
                this.jugador3.setControllerProfile(player3.getControllerProfile());
            }
        }

        if (player4 != null) {
            MapObject startPoint4 = tma.getRectangleList("Start").get(obtenerPuntoInicio());
            MapProperties startP4Props = startPoint4.getProperties();
            this.jugador4 = new Player((float) startP4Props.get("x"), (float) startP4Props.get("y"), mainStage,
                    player4.getnColor(), player4.getnSkin(), player4.isCPU());
            this.jugador4.setnPlayer(4);
            this.jugador4.setPlayerName(player4.getPlayerName(), uiStage);
            if (!player4.isCPU()) {
                this.jugador4.setController(player4.getController());
                this.jugador4.setControllerProfile(player4.getControllerProfile());
            }
        }
    }

    private void generarArmas(TiledMapActor tma) {
        for (MapObject obj : tma.getRectangleList("Weapon")) {
            MapProperties props = obj.getProperties();
            new WeaponSpawner((float) props.get("x"), (float) props.get("y"), mainStage);
        }
    }

    private void generarTrampolines(TiledMapActor tma) {
        for (MapObject obj : tma.getRectangleList("Trampoline")) {
            MapProperties props = obj.getProperties();
            new Trampoline((float) props.get("x"), (float) props.get("y"), mainStage);
        }
    }

    private void generarCajas(TiledMapActor tma) {
        for (MapObject obj : tma.getRectangleList("Crate")) {
            MapProperties props = obj.getProperties();
            new Crate((float) props.get("x"), (float) props.get("y"), 36,
                    36, mainStage);
        }
    }

    private void generarPlataformas(TiledMapActor tma) {
        for (MapObject obj : tma.getRectangleList("Platform")) {
            MapProperties props = obj.getProperties();
            new Platform((float) props.get("x"), (float) props.get("y"), (float) props.get("width"),
                    (float) props.get("height"), mainStage);
        }
    }

    private void generarTerreno(TiledMapActor tma) {
        for (MapObject obj : tma.getRectangleList("Solid")) {
            MapProperties props = obj.getProperties();
            new Solid((float) props.get("x"), (float) props.get("y"), (float) props.get("width"),
                    (float) props.get("height"), mainStage);
        }
    }

    private int obtenerPuntoInicio() {
        int rand;
        boolean uniqueStart;

        do {
            uniqueStart = true;
            rand = new Random().nextInt(4);
            for (int sp : puntosInicioEnUso) {
                if (sp == rand) {
                    uniqueStart = false;
                    break;
                }
            }
        } while (!uniqueStart);

        puntosInicioEnUso.add(rand);
        return rand;
    }

    @Override
    public boolean axisMoved(Controller controller, int i, float v) {
        if (juegoListo) {
            try {
                for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                    Player player = (Player) playerActor;

                    if (player.getController().equals(controller)) {
                        if (i == player.getControllerProfile().getJoystickX()) {
                            if (v == 1)
                                player.moveRight();
                            else if (v == -1)
                                player.moveLeft();
                            else
                                player.decelerate();
                        }
                        if (i == player.getControllerProfile().getJoystickY()) {
                            if (v == 1) {
                                if (player.isOnPlatform())
                                    player.goDown(true);
                            } else
                                player.goDown(false);
                        }
                        break;
                    }
                }
            } catch (NullPointerException ex) {
                // Controla la posibilidad de que se registre una pulsación de un mando no asociado a un jugador
            }
        }
        return false;
    }

    @Override
    public boolean buttonDown(Controller controller, int i) {
        if (juegoListo) {
            try {
                for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                    Player player = (Player) playerActor;

                    if (player.getController().equals(controller)) {
                        if (i == player.getControllerProfile().getButtonJump()) {
                            player.jump();
                            player.setOnGround(false);
                        }
                        if (i == player.getControllerProfile().getButtonShoot()) {
                            player.shoot();
                        }
                        if (i == player.getControllerProfile().getButtonGrab()) {
                            player.grabItem();
                        }
                        if (i == player.getControllerProfile().getButtonThrow()) {
                            player.throwItem();
                        }
                        break;
                    }
                }
            } catch (NullPointerException ex) {
                // Controla la posibilidad de que se registre una pulsación de un mando no asociado a un jugador
            }
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int i) {
        if (juegoListo) {
            try {
                for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                    Player player = (Player) playerActor;

                    if (player.getController().equals(controller)) {
                        if (i == player.getControllerProfile().getButtonShoot()) {
                            player.stopShooting();
                        }
                        break;
                    }
                }
            } catch (NullPointerException ex) {
                // Controla la posibilidad de que se registre una pulsación de un mando no asociado a un jugador
            }
        }
        return false;
    }

    @Override
    public void update(float dt) {
        mostrarCuentaAtras();

        // Colisiones
        reiniciarColisionJugadores();
        colisionMovilSuelo();
        colisiomMovilPlataforma();
        colisionJugadorMovil();
        colisionMovilTrampolin();
        colisionArmasSpawner();
        colisionProyectiles();

        procesarMovimientoCPU();

        reiniciarGravedadJugadores();

        comprobarFinDeRonda();

        muerteFueraDelMapa();

        evitarBloqueoIA();
    }

    private void reiniciarGravedadJugadores() {
        for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
            if (mobileActor.getName().equalsIgnoreCase("player")) {
                Player playerActor = (Player) mobileActor;
                if (!playerActor.isOnPlatform() && !playerActor.isOnGround()) {
                    playerActor.setGravityEnabled(true);
                }
            } else if (mobileActor.getName().equalsIgnoreCase("crate")) {
                Crate crateActor = (Crate) mobileActor;
                boolean feetColision = false;
                for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
                    if (crateActor.getFeetSensor().overlaps(solidActor))
                        feetColision = true;
                }
                for (BaseActor platformActor : BaseActor.getList(mainStage, Platform.class.getName())) {
                    if (crateActor.getFeetSensor().overlaps(platformActor))
                        feetColision = true;
                }
                if (!feetColision)
                    crateActor.setGravityEnabled(true);
            }
        }
    }

    private void evitarBloqueoIA() {
        if (!jugador1.isAlive() && jugador2.isCPU() && !inicioAntiBloqueo) {
        inicioAntiBloqueo = true;
        inicioSoloIA = System.currentTimeMillis();
        }

        if (inicioAntiBloqueo) {
            ArrayList<Player> alivePlayers = new ArrayList<>();

            long currentOnlyIA = System.currentTimeMillis();
            if (inicioSoloIA - currentOnlyIA < -tiempoEsperaMuerte) {
                for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                    Player player = (Player) playerActor;
                    if (player.isAlive()) {
                        alivePlayers.add(player);
                    }
                }
                if (alivePlayers.size() > 0) {
                    alivePlayers.get(alivePlayers.size() - 1).die(DeadType.VOID);
                    alivePlayers.remove(alivePlayers.size() - 1);
                    tiempoEsperaMuerte += 3000;
                }
            }
        }
    }

    private void muerteFueraDelMapa() {
        for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
            Player player = (Player) playerActor;
            if (player.isAlive() && player.getY() < -200)
                player.die(DeadType.VOID);
        }
    }

    private void comprobarFinDeRonda() {
        if (!rondaAcabando) {
            int alivePlayers = 0;
            for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                Player player = (Player) playerActor;
                if (player.isAlive())
                    alivePlayers++;
            }
            if (alivePlayers <= 1) {
                rondaAcabando = true;
                tiempoFinDeRonda = System.currentTimeMillis();
            }

        } else {
            long currentTimeEnding = System.currentTimeMillis();
            if (tiempoFinDeRonda - currentTimeEnding < -5000) {
                iniciarSiguienteRonda();
            } else if (tiempoFinDeRonda - currentTimeEnding < -3000 && !finDeRonda) {
                obtenerPunto();
                finDeRonda = true;
            }
        }
    }

    private void iniciarSiguienteRonda() {
        if (GameScore.currentRound == GameScore.totalRounds) {
            GameScore.currentRound = 0;
            GameScore.totalRounds = 0;
            PFGGame.setActiveScreen(new VictoryScreen(jugador1, jugador2, jugador3, jugador4));
        } else {
            GameScore.currentRound++;
            PFGGame.setActiveScreen(new LevelScreen(jugador1, jugador2, jugador3, jugador4, nNivel + 1));
        }
    }

    private void obtenerPunto() {
        Label scoreLabel = new Label("+1", BaseGame.styleNormal);
        scoreLabel.setPosition(-100, -100);
        uiStage.addActor(scoreLabel);
        if (jugador1.isAlive()) {
            GameScore.scoreP1++;
            scoreLabel.setPosition(jugador1.getX() + (jugador1.getWidth() / 2) - (scoreLabel.getWidth() / 2),
                    jugador1.getY() + 150);
            puntoObtenido.play(Config.soundVolume);
        }
        if (jugador2.isAlive()) {
            GameScore.scoreP2++;
            scoreLabel.setPosition(jugador2.getX() + (jugador2.getWidth() / 2) - (scoreLabel.getWidth() / 2),
                    jugador2.getY() + 150);
            puntoObtenido.play(Config.soundVolume);
        }
        if (jugador3 != null) {
            if (jugador3.isAlive()) {
                GameScore.scoreP3++;
                scoreLabel.setPosition(jugador3.getX() + (jugador3.getWidth() / 2) - (scoreLabel.getWidth() / 2),
                        jugador3.getY() + 150);
                puntoObtenido.play(Config.soundVolume);
            }
        }
        if (jugador4 != null) {
            if (jugador4.isAlive()) {
                GameScore.scoreP4++;
                scoreLabel.setPosition(jugador4.getX() + (jugador4.getWidth() / 2) - (scoreLabel.getWidth() / 2),
                        jugador4.getY() + 150);
                puntoObtenido.play(Config.soundVolume);
            }
        }
        scoreLabel.addAction(Actions.delay(1));
        scoreLabel.addAction(Actions.after(Actions.fadeOut(0.5f)));
        pantallaCarga.addAction(Actions.delay(1.5f));
        pantallaCarga.addAction(Actions.after(Actions.fadeIn(0.5f)));
    }

    private void colisionProyectiles() {
        for (BaseActor proyectileActor : BaseActor.getList(mainStage, Proyectile.class.getName())) {
            Proyectile pr = (Proyectile) proyectileActor;
            for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                Player player = (Player) playerActor;
                if (proyectileActor.overlaps(player) && player.isAlive()) {
                    player.die(DeadType.SHOT);
                    if (pr.getnProyectile() != 8)
                        proyectileActor.remove();
                }
            }
            for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
                if (proyectileActor.overlaps(solidActor)) {
                    proyectileActor.remove();
                }
            }
            for (BaseActor crateActor : BaseActor.getList(mainStage, Crate.class.getName())) {
                if (proyectileActor.overlaps(crateActor)) {
                    Crate crate = (Crate) crateActor;
                    proyectileActor.remove();
                    crate.destroy();
                }
            }
        }
    }

    private void procesarMovimientoCPU() {
        if (juegoListo) {
            for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                Player player = (Player) playerActor;
                if (player.isCPU() && player.isAlive()) {
                    int nAlivePlayers = 0;
                    for (BaseActor playerC : BaseActor.getList(mainStage, Player.class.getName())) {
                        Player playerCA = (Player) playerC;
                        if (playerCA.isAlive())
                            nAlivePlayers++;
                    }
                    if (nAlivePlayers > 1) {
                        try {
                            switch (player.getnPlayer()) {
                                case 2:
                                    if (jugador2.getWeapon() == null) {
                                        cogerArmaCPU(jugador2);
                                        patearJugadorCPU(jugador2);
                                        perseguirActorCPU(jugador2, armaMasCercanaCPU(jugador2));
                                    } else {
                                        perseguirActorCPU(jugador2, jugadorMasCercanoCPU(jugador2));
                                        dispararJugadorEnRangoCPU(jugador2);
                                    }
                                    break;

                                case 3:
                                    if (jugador3.getWeapon() == null) {
                                        cogerArmaCPU(jugador3);
                                        patearJugadorCPU(jugador3);
                                        escaparDelActorCPU(jugador3, jugadorMasCercanoCPU(jugador3));
                                    } else {
                                        perseguirActorCPU(jugador3, jugadorMasCercanoCPU(jugador3));
                                        dispararJugadorEnRangoCPU(jugador3);
                                    }
                                    break;

                                case 4:
                                    perseguirActorCPU(jugador4, jugadorMasCercanoCPU(jugador4));
                                    patearJugadorCPU(jugador4);
                                    break;
                            }
                        } catch (NullPointerException np) {
                            // Controlla que se apunte a direcciones de memoria vacías al intentar interacturar con
                            // objetos que ya no existen.
                        }
                    } else {
                        player.decelerate();
                    }
                }
            }
        }
    }

    private void colisionArmasSpawner() {
        for (BaseActor weaponSpawnerActor : BaseActor.getList(mainStage, WeaponSpawner.class.getName())) {
            WeaponSpawner wp = (WeaponSpawner) weaponSpawnerActor;
            for (BaseActor weaponItemActor : BaseActor.getList(mainStage, WeaponItem.class.getName())) {
                WeaponItem wi = (WeaponItem) weaponItemActor;
                if (wi.overlaps(wp) && !wp.itHasWeapon()) {
                    wp.setWeapon(wi);
                }
            }
        }
    }

    private void colisionMovilTrampolin() {
        for (BaseActor trampolineActor : BaseActor.getList(mainStage, Trampoline.class.getName())) {
            Trampoline trampoline = (Trampoline) trampolineActor;
            for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
                if (mobileActor.getName().equalsIgnoreCase("Player")) {
                    Player player = (Player) mobileActor;
                    if (player.getFeetSensorR().overlaps(trampoline) || player.getFeetSensorL().overlaps(trampoline))
                        trampoline.jump(player);
                    else if (player.overlaps(trampoline))
                        player.preventOverlap(trampoline);
                } else if (mobileActor.getName().equalsIgnoreCase("Crate")) {
                    Crate crate = (Crate) mobileActor;
                    if (crate.overlaps(trampoline)) {
                        trampoline.jump(crate);
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("Weapon")) {
                    WeaponItem weaponItem = (WeaponItem) mobileActor;
                    if (weaponItem.overlaps(trampoline)) {
                        trampoline.jump(weaponItem);
                    }
                }
            }
        }
    }

    private void colisionJugadorMovil() {
        for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
            Player player = (Player) playerActor;

            if (player.isAlive()) {
                for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
                    if (mobileActor.getName().equalsIgnoreCase("Crate")) {
                        Crate crate = (Crate) mobileActor;
                        if (player.isKickingNow()) {
                            if (player.getKickBox().overlaps(crate)) {
                                crate.reciveKick(player.getDirection());
                            }
                        }

                        if (player.isWantsToGrab()) {
                            if (player.overlaps(crate.getGrabSensor())) {
                                crate.remove();
                                player.setWeapon(0);
                                break;
                            }
                        }

                        if (crate.getFeetSensor().overlaps(player) && crate.getVelocityVector().y < 0) {
                            crate.destroy();
                            player.die(DeadType.CRUSHED);
                            break;
                        }

                        if (player.getY() <= crate.getY() + crate.getHeight()) {
                            if (player.getFeetSensorL().overlaps(crate) && player.getFeetSensorR().overlaps(crate)) {
                                player.setPosition(player.getX(), crate.getY() + crate.getHeight() + 2);
                            }
                        }

                        if (player.getY() > crate.getY() + crate.getHeight()) {
                            if (player.getFeetSensorL().overlaps(crate) || player.getFeetSensorR().overlaps(crate)) {
                                player.setGravityEnabled(false);
                                player.setOnGround(true);
                            }
                        } else {
                            if (player.overlaps(crate)) {
                                crate.move(player.getVelocityVector().x / 2, 0);
                                player.preventOverlap(crate);
                            }
                        }
                    } else if (mobileActor.getName().equalsIgnoreCase("weapon")) {
                        WeaponItem weaponItemActor = (WeaponItem) mobileActor;
                        if (player.isWantsToGrab()) {
                            if (player.overlaps(weaponItemActor.getGrabSensor()) && player.getWeapon() == null) {
                                player.setWeapon(weaponItemActor.getNWeapon());
                                if (weaponItemActor.getnMunition() != -1)
                                    player.getWeapon().setnMunition(weaponItemActor.getnMunition());
                                for (BaseActor weaponSpawnerActor : BaseActor.getList(mainStage, WeaponSpawner.class.getName())) {
                                    if (weaponItemActor.overlaps(weaponSpawnerActor)) {
                                        WeaponSpawner wp = (WeaponSpawner) weaponSpawnerActor;
                                        wp.removeWeapon();
                                    }
                                }
                                weaponItemActor.remove();
                            }
                        }
                    } else if (mobileActor.getName().equalsIgnoreCase("player")) {
                        Player enemyPlayer = (Player) mobileActor;
                        if (player.isKickingNow() && enemyPlayer != player) {
                            if (player.getKickBox().overlaps(enemyPlayer)) {
                                enemyPlayer.reciveKick(player.getDirection());
                            }
                        }
                    }
                }
            }
        }
    }

    private void colisiomMovilPlataforma() {
        for (BaseActor platformActor : BaseActor.getList(mainStage, Platform.class.getName())) {
            for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
                if (mobileActor.getName().equalsIgnoreCase("player")) {
                    Player playerActor = (Player) mobileActor;
                    if (playerActor.isAlive()) {
                        if (playerActor.getVelocityVector().y <= 0 && playerActor.isGravityLock()) {
                            if (playerActor.getY() > platformActor.getY() + platformActor.getHeight() - 10 &&
                                    playerActor.getY() <= platformActor.getY() + platformActor.getHeight()) {
                                if (playerActor.getFeetSensorL().overlaps(platformActor) ||
                                        playerActor.getFeetSensorR().overlaps(platformActor)) {
                                    playerActor.setPosition(playerActor.getX(), platformActor.getY() +
                                            platformActor.getHeight() + 2);
                                }
                            }

                            if (playerActor.getY() > platformActor.getY() + platformActor.getHeight()) {
                                if (playerActor.getFeetSensorL().overlaps(platformActor) ||
                                        playerActor.getFeetSensorR().overlaps(platformActor)) {
                                    playerActor.setGravityEnabled(false);
                                    playerActor.setOnGround(true);
                                    playerActor.setOnPlatform(true);
                                }
                            }
                        }
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("crate")) {
                    Crate crateActor = (Crate) mobileActor;
                    if (crateActor.getVelocityVector().y <= 0) {
                        if (crateActor.getY() > platformActor.getY() + platformActor.getHeight() - 10 &&
                                crateActor.getY() <= platformActor.getY() + platformActor.getHeight()) {
                            if (crateActor.getFeetSensor().overlaps(platformActor)) {
                                crateActor.setPosition(crateActor.getX(), platformActor.getY() +
                                        platformActor.getHeight() + 2);
                            }
                        }

                        if (crateActor.getY() > platformActor.getY() + platformActor.getHeight()) {
                            if (crateActor.getFeetSensor().overlaps(platformActor)) {
                                crateActor.setGravityEnabled(false);
                            }
                        }
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("weapon")) {
                    WeaponItem weaponItemActor = (WeaponItem) mobileActor;
                    if (weaponItemActor.getVelocityVector().y <= 0) {
                        if (weaponItemActor.getY() > platformActor.getY() + platformActor.getHeight() - 10 &&
                                weaponItemActor.getY() <= platformActor.getY() + platformActor.getHeight()) {
                            if (weaponItemActor.getFeetSensor().overlaps(platformActor)) {
                                weaponItemActor.setPosition(weaponItemActor.getX(), platformActor.getY() +
                                        platformActor.getHeight() + 2);
                            }
                        }

                        if (weaponItemActor.getY() > platformActor.getY() + platformActor.getHeight()) {
                            if (weaponItemActor.getFeetSensor().overlaps(platformActor)) {
                                weaponItemActor.setGravityEnabled(false);
                            }
                        }
                    }
                }
            }
        }
    }

    private void colisionMovilSuelo() {
        for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
            for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
                if (mobileActor.getName().equalsIgnoreCase("player")) {
                    Player playerActor = (Player) mobileActor;
                    if (playerActor.isAlive()) {
                        if (playerActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                            if (playerActor.getFeetSensorL().overlaps(solidActor) && playerActor.getFeetSensorR().overlaps(solidActor)) {
                                playerActor.setPosition(playerActor.getX(), solidActor.getY() + solidActor.getHeight() + 2);
                            }
                        }

                        if (playerActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                            if (playerActor.getFeetSensorL().overlaps(solidActor) || playerActor.getFeetSensorR().overlaps(solidActor)) {
                                playerActor.setGravityEnabled(false);
                                playerActor.setOnGround(true);
                            }
                        } else {
                            playerActor.preventOverlap(solidActor);
                        }
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("crate")) {
                    Crate crateActor = (Crate) mobileActor;
                    if (crateActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                        if (crateActor.getFeetSensor().overlaps(solidActor)) {
                            crateActor.setPosition(crateActor.getX(), solidActor.getY() + solidActor.getHeight() + 1);
                        }
                    }

                    if (crateActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                        if (crateActor.getFeetSensor().overlaps(solidActor)) {
                            crateActor.setGravityEnabled(false);
                        }
                    } else {
                        crateActor.preventOverlap(solidActor);
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("weapon")) {
                    WeaponItem weaponItemActor = (WeaponItem) mobileActor;
                    if (weaponItemActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                        if (weaponItemActor.getFeetSensor().overlaps(solidActor)) {
                            weaponItemActor.setPosition(weaponItemActor.getX(), solidActor.getY() + solidActor.getHeight() + 1);
                        }
                    }

                    if (weaponItemActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                        if (weaponItemActor.getFeetSensor().overlaps(solidActor)) {
                            weaponItemActor.setGravityEnabled(false);
                        }
                    } else {
                        weaponItemActor.preventOverlap(solidActor);
                    }
                }
            }
        }
    }

    private void reiniciarColisionJugadores() {
        for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
            if (mobileActor.getName().equalsIgnoreCase("player")) {
                Player playerActor = (Player) mobileActor;
                playerActor.setOnGround(false);
                playerActor.setOnPlatform(false);
            }
        }
    }

    private void mostrarCuentaAtras() {
        if (!juegoListo) {
            long currentPlayTime = System.currentTimeMillis();
            long time = tiempoCuentaAtras - currentPlayTime;
            if (time < -2500 && numeroCuentaAtras == 0) {
                juegoListo = true;
                cuentaAtras.setAnimation(cuentaAtras.loadTexture("src/apalacios/pfg/assets/count_down/ya.png"));
                sonidoInicio.play(Config.soundVolume);
                cuentaAtras.addAction(Actions.fadeOut(0.5f));
            } else if (time < -2000 && numeroCuentaAtras == 1) {
                cuentaAtras.setAnimation(cuentaAtras.loadTexture("src/apalacios/pfg/assets/count_down/numero1.png"));
                sonidoCuentaAtras.play(Config.soundVolume);
                numeroCuentaAtras--;
            } else if (time < -1500 && numeroCuentaAtras == 2) {
                cuentaAtras.setAnimation(cuentaAtras.loadTexture("src/apalacios/pfg/assets/count_down/numero2.png"));
                sonidoCuentaAtras.play(Config.soundVolume);
                numeroCuentaAtras--;
            } else if (time < -1000 && numeroCuentaAtras == 3) {
                cuentaAtras.addAction(Actions.fadeIn(0));
                sonidoCuentaAtras.play(Config.soundVolume);
                numeroCuentaAtras--;
            }
        }
    }

    /**
     * Localiza el arma (WeaponItem) más cercana al pinguino enemigo (CPU)
     *
     * @param cpu representa al pinguino enemigo
     * @return WeaponItem más cercano a las coordenadas del pinguino enemigo
     */
    private WeaponItem armaMasCercanaCPU(Player cpu) {
        WeaponItem currentWeapon = null;
        int weaponDistance = 99999;

        for (BaseActor weaponItemActor : BaseActor.getList(mainStage, WeaponItem.class.getName())) {
            WeaponItem weaponItem = (WeaponItem) weaponItemActor;
            int tempDistance = 0;

            if (currentWeapon == null)
                currentWeapon = weaponItem;

            if (weaponItem.getnMunition() > 0) {
                if (cpu.getX() >= weaponItem.getX()) {
                    tempDistance += (cpu.getX() - weaponItem.getX());
                } else {
                    tempDistance += (weaponItem.getX() - cpu.getX());
                }

                if (cpu.getY() >= weaponItem.getY()) {
                    tempDistance += (cpu.getY() - weaponItem.getY());
                } else {
                    tempDistance += (weaponItem.getY() - cpu.getY());
                }

                if (tempDistance < weaponDistance) {
                    currentWeapon = weaponItem;
                    weaponDistance = tempDistance;
                }
            }
        }
        return currentWeapon;
    }

    /**
     * Localiza el jugador (humano o CPU) más cercano al enemigo (CPU)
     *
     * @param cpu representa al pinguino enemigo
     * @return Player más cercano a las coordenadas del pinguino enemigo
     */
    private Player jugadorMasCercanoCPU(Player cpu) {
        Player currentPlayer = null;
        int playerDistance = 99999;

        for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
            Player player = (Player) playerActor;
            int tempDistance = 0;

            if (player != cpu && player.isAlive()) {
                if (currentPlayer == null)
                    currentPlayer = player;

                if (cpu.getX() >= player.getX()) {
                    tempDistance += (cpu.getX() - player.getX());
                } else {
                    tempDistance += (player.getX() - cpu.getX());
                }

                if (cpu.getY() >= player.getY()) {
                    tempDistance += (cpu.getY() - player.getY());
                } else {
                    tempDistance += (player.getY() - cpu.getY());
                }

                if (tempDistance < playerDistance) {
                    currentPlayer = player;
                    playerDistance = tempDistance;
                }
            }
        }
        return currentPlayer;
    }

    /**
     * Recoge el arma (WeaponItem) que este en contacto con el pinguino enemigo (CPU)
     *
     * @param cpu representa al pinguino enemigo
     */
    private void cogerArmaCPU(Player cpu) {
        for (BaseActor weaponItemActor : BaseActor.getList(mainStage, WeaponItem.class.getName())) {
            WeaponItem weaponItem = (WeaponItem) weaponItemActor;

            if (cpu.overlaps(weaponItem.getGrabSensor()) && cpu.getWeapon() == null) {
                cpu.setWeapon(weaponItem.getNWeapon());
                weaponItemActor.remove();
            }
        }
    }

    /**
     * Determina si existe algún jugador al alcance de disparo y dispara el arma que tenga equipada.
     * En caso de que el arma no tenga munición la tirará al suelo
     *
     * @param cpu representa al pinguino enemigo
     */
    private void dispararJugadorEnRangoCPU(Player cpu) {
        if (cpu.getWeapon() != null) {
            if (cpu.getWeapon().nMunition > 0) {
                for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                    Player player = (Player) playerActor;

                    // Determinar si el objetivo no es el mismo enemigo (CPU) y si se encuentra a una altura similar
                    if (cpu != player && cpu.getY() - player.getY() < 20 && cpu.getY() - player.getY() > -20 && player.isAlive()) {
                        for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {

                            // Determinar si el muro está a una altura que no interfiera en el disparo
                            if (cpu.getY() > solidActor.getY() + solidActor.getHeight()) {

                                if (cpu.getX() > player.getX())
                                    cpu.moveLeft();
                                else
                                    cpu.moveRight();
                                if (cpu.getWeapon().nMunition > 0) {
                                    cpu.decelerate();
                                    cpu.shoot();
                                } else {
                                    cpu.throwItem();
                                }
                            }
                        }
                    }
                }
            } else {
                cpu.throwItem();
            }
        }
    }

    private void patearJugadorCPU(Player cpu) {
        for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
            Player player = (Player) playerActor;

            if (player.isAlive() && player != cpu && player.overlaps(cpu)) {
                cpu.shoot();
            }
        }
    }

    /**
     * Desplaza al enemigo (CPU) hacia el objeto deseado (actor), evitando los obstáculos
     * En ocasiones puede dejar al enemigo (CPU) atascado dependiendo de la disposición del mapa
     *
     * @param cpu representa el pinguino enemigo
     * @param actor representa el objeto al que el pinguino enemigo debe dirigirse
     */
    private void perseguirActorCPU(Player cpu, BaseActor actor) throws NullPointerException {

        // Moverse en dirección al objeto deseado (izquierda - derecha)
        if (cpu.getX() > actor.getX())
            cpu.moveLeft();
        else
            cpu.moveRight();

        // El objeto se encuentra encima del enemigo (CPU)
        if (cpu.getY() < actor.getY() && actor.getY() - cpu.getY() > 20) {
            for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
                if (cpu.getDirection() == Direction.RIGHT) {

                    // Determina si delante del enemigo (CPU) hay un muro (Solid) a una distancia no
                    // superio a 96 px (2 tiles) y si este muro tiene una altura respecto a la del
                    // enemigo (CPU), no superor a 300 px.
                    if (solidActor.getX() >= (cpu.getX() + cpu.getWidth()) &&
                            solidActor.getX() - (cpu.getX() + cpu.getWidth()) < 96 &&
                            (solidActor.getY() + solidActor.getHeight()) - cpu.getY() < 300) {

                        // Si el muro cumple todas las condiciones, se determina si este se encuentra
                        // entre el enemigo (CPU) y el objeto deseado.
                        if (actor.getX() >= solidActor.getX()) {
                            cpu.jump();
                        } else {
                            cpu.decelerate();
                        }
                    }
                } else {
                    if ((solidActor.getX() + solidActor.getWidth()) <= cpu.getX() &&
                            cpu.getX() - (solidActor.getX() + solidActor.getWidth()) < 96 &&
                            (solidActor.getY() + solidActor.getHeight()) - cpu.getY() < 300) {
                        if (actor.getX() <= (solidActor.getX() + solidActor.getWidth())) {
                            cpu.jump();
                        } else {
                            cpu.decelerate();
                        }
                    }
                }
            }

            Platform currentPlatform = null;
            float distance = 99999;

            for (BaseActor platformActor : BaseActor.getList(mainStage, Platform.class.getName())) {
                Platform platform = (Platform) platformActor;

                float tempDistance;

                // Determina si hay una plataforma (Platform) por encima del enemigo (CPU), pero no
                // a más de 150 píxeles (unos 3 tiles)
                if (platform.getY() + platform.getHeight() >= cpu.getY() &&
                        (platform.getY() + platform.getHeight()) - cpu.getY() <= 150) {

                    if (cpu.getX() > platform.getX() + platform.getWidth()) {
                        tempDistance = cpu.getX() - (platform.getX() + platform.getWidth());
                    } else {
                        tempDistance = platform.getX() - (cpu.getX() + cpu.getWidth());
                    }

                    // Entre las plataformas que cumplan la condición de altura, selecciona
                    // la más cercana al enemigo (CPU)
                    if (tempDistance < distance) {
                        currentPlatform = platform;
                        distance = tempDistance;
                    }
                }
            }

            // Desplaza al enemigo (CPU) a la plataforma más cercana
            if (currentPlatform != null) {
                if (cpu.getX() + (cpu.getWidth() / 2) < currentPlatform.getX() +
                        (currentPlatform.getWidth() / 2)) {
                    cpu.moveRight();
                } else {
                    cpu.moveLeft();
                }
                if ((currentPlatform.getY() + currentPlatform.getHeight()) - cpu.getY() >= 20) {
                    cpu.jump();
                }
            }

        // El objeto se encuentra debajo del enemigo (CPU)
        } else if (cpu.getY() > actor.getY() && cpu.getY() - actor.getY() > 20) {
            if (cpu.isOnPlatform()) {
                cpu.goDown(true);
                cpu.jump();
            } else {

                Platform currentPlatform = null;
                float distance = 99999;

                for (BaseActor platformActor : BaseActor.getList(mainStage, Platform.class.getName())) {
                    Platform platform = (Platform) platformActor;

                    float tempDistance;

                    // Determina si hay una plataforma (Platform) a la misma altura que el
                    // enemigo (CPU),
                    if (platform.getY() + platform.getHeight() <= cpu.getY() &&
                        cpu.getY() - (platform.getY() + platform.getHeight()) < 20) {

                        if (cpu.getX() > platform.getX() + platform.getWidth()) {
                            tempDistance = cpu.getX() - (platform.getX() + platform.getWidth());
                        } else {
                            tempDistance = platform.getX() - (cpu.getX() + cpu.getWidth());
                        }

                        // Entre las plataformas que cumplan la condición de altura, selecciona
                        // la más cercana al enemigo (CPU)
                        if (tempDistance < distance) {
                            currentPlatform = platform;
                            distance = tempDistance;
                        }
                    }
                }

                // Desplaza al enemigo (CPU) a la plataforma más cercana
                if (currentPlatform != null) {
                    if (cpu.getX() + (cpu.getWidth() / 2) < currentPlatform.getX() +
                            (currentPlatform.getWidth() / 2)) {
                        cpu.moveRight();
                    } else {
                        cpu.moveLeft();
                    }
                }
            }
        }

        // Si el enemigo (CPU) se encuentra andando contra un muro, salta tratando de sobrepasarlo
        for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
            if (cpu.getDirection() == Direction.RIGHT) {
                if (solidActor.getX() - (cpu.getX() + cpu.getWidth()) < 5 &&
                        solidActor.getX() - (cpu.getX() + cpu.getWidth()) > -1) {
                    cpu.jump();
                    break;
                }
            } else {
                if (cpu.getX() - (solidActor.getX() + solidActor.getWidth()) < 5 &&
                        cpu.getX() - (solidActor.getX() + solidActor.getWidth()) > -1) {
                    cpu.jump();
                    break;
                }
            }
        }
    }

    private void escaparDelActorCPU(Player cpu, BaseActor actor) {
        if (cpu.getX() > actor.getX())
            cpu.moveRight();
        else
            cpu.moveLeft();

        // Si el enemigo (CPU) se encuentra andando contra un muro, salta tratando de sobrepasarlo
        for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
            if (cpu.getDirection() == Direction.RIGHT) {
                if (solidActor.getX() - (cpu.getX() + cpu.getWidth()) < 5 &&
                        solidActor.getX() - (cpu.getX() + cpu.getWidth()) > -1) {
                    cpu.jump();
                    break;
                }
            } else {
                if (cpu.getX() - (solidActor.getX() + solidActor.getWidth()) < 5 &&
                        cpu.getX() - (solidActor.getX() + solidActor.getWidth()) > -1) {
                    cpu.jump();
                    break;
                }
            }
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
        mainStage.getViewport().update(arg0, arg1, true);
    }
}