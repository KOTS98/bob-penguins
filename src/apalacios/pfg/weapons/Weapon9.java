package apalacios.pfg.weapons;

import apalacios.pfg.characters.Weapon;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Weapon9 extends Weapon {


    public Weapon9(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 9, -1);
    }

    @Override
    public void shoot() {

    }

    @Override
    public void stopShooting() {

    }

    @Override
    public void reload() {

    }
}
