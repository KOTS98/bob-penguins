package apalacios.pfg.characters;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public abstract class Mobile extends BaseActor {

    public Mobile(float posX, float posY, Stage stage) {
        super(posX, posY, stage);
    }

    public abstract void trampolineJump();

    @Override
    public boolean remove() {
        super.remove();
        addAction(Actions.removeActor());
        return false;
    }

    @Override
    public void act(float dt) {
        super.act(dt);
    }
}
