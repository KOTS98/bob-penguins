package apalacios.pfg.characters;

import apalacios.pfg.util.Direction;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Proyectile extends BaseActor {

    private Direction direction;
    private int velocityX;
    private boolean gravity;
    private float gravityForce;
    private float maxVerticalSpeed;
    private int lifeTime;
    private long startTime;
    private int nProyectile;

    public Proyectile(float posX, float posY, Stage stage, int velocityX, Direction direction, boolean gravity,
                      int nProyectile, int lifeTime) {
        super(posX, posY, stage);
        startTime = System.currentTimeMillis();
        this.velocityX = velocityX;
        this.direction = direction;
        this.gravity = gravity;
        this.lifeTime = lifeTime;
        this.nProyectile = nProyectile;

        gravityForce = 20;
        maxVerticalSpeed = 500;

        if (direction == Direction.RIGHT)
            loadTexture("src/apalacios/pfg/assets/proyectiles/pr" + nProyectile + "/right.png");
        else
            loadTexture("src/apalacios/pfg/assets/proyectiles/pr" + nProyectile + "/left.png");
    }

    public int getnProyectile() {
        return nProyectile;
    }

    @Override
    public void act(float dt) {
        super.act(dt);

        // Movimiento horizontal
        if (direction == Direction.RIGHT)
            velocityVec.set(velocityX, velocityVec.y);
        else
            velocityVec.set(-velocityX, velocityVec.y);

        if (gravity)
            velocityVec.add(0, -gravityForce);

        velocityVec.y = MathUtils.clamp(velocityVec.y, -maxVerticalSpeed, maxVerticalSpeed);
        moveBy(Math.round(velocityVec.x * dt), Math.round(velocityVec.y * dt));

        if (getX() > 1930 || getX() < -10 || getY() < - 10 || (getY() > 1090 && !gravity)) {
            this.remove();
        }

        long currentTime = System.currentTimeMillis();
        if (startTime - currentTime < -lifeTime) {
            setPosition(-100, -100);
            remove();
        }
    }
}
