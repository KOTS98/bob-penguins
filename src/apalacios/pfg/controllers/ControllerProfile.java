package apalacios.pfg.controllers;

import java.io.Serializable;

public class ControllerProfile implements Serializable {
    private String controllerType;

    private int joystickX;
    private int joystickY;

    private int buttonJump;
    private int buttonGrab;
    private int buttonThrow;
    private int buttonShoot;
    private int buttonStart;

    public String getControllerType() {
        return controllerType;
    }

    public void setControllerType(String controllerType) {
        this.controllerType = controllerType;
    }

    public int getJoystickX() {
        return joystickX;
    }

    public void setJoystickX(int joystickX) {
        this.joystickX = joystickX;
    }

    public int getJoystickY() {
        return joystickY;
    }

    public void setJoystickY(int joystickY) {
        this.joystickY = joystickY;
    }

    public int getButtonJump() {
        return buttonJump;
    }

    public void setButtonJump(int buttonJump) {
        this.buttonJump = buttonJump;
    }

    public int getButtonGrab() {
        return buttonGrab;
    }

    public void setButtonGrab(int buttonGrab) {
        this.buttonGrab = buttonGrab;
    }

    public int getButtonThrow() {
        return buttonThrow;
    }

    public void setButtonThrow(int buttonThrow) {
        this.buttonThrow = buttonThrow;
    }

    public int getButtonShoot() {
        return buttonShoot;
    }

    public void setButtonShoot(int buttonShoot) {
        this.buttonShoot = buttonShoot;
    }

    public int getButtonStart() {
        return buttonStart;
    }

    public void setButtonStart(int buttonStart) {
        this.buttonStart = buttonStart;
    }
}
