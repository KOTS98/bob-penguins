package apalacios.pfg.util;

import apalacios.pfg.screens.ProfilesScreen;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.util.ArrayList;

public class Util {
    public static LastScreen lastScreen;

    public static ArrayList<Profile> loadProfiles() {
        ArrayList<Profile> profiles;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("src/apalacios/pfg/profiles.bpp"));
            profiles = (ArrayList<Profile>) ois.readObject();
            System.out.println("[ OK ] Archivo de perfiles cargado con exito!");
        } catch (Exception ex) {
            // Controla cualquier error de lectura del fichero, escribiendo uno nuevo vacío
            System.out.println("[INFO] Archivo de perfiles no encontrado, creando uno nuevo...");
            profiles = new ArrayList<>();
        }
        return profiles;
    }

    public static void saveProfiles(ArrayList<Profile> profiles) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("src/apalacios/pfg/profiles.bpp"));
            oos.writeObject(profiles);
            oos.close();
            System.out.println("[ OK ] Configuración de perfiles guardada con exito! > src/apalacios/pfg/profiles.bpp");
        } catch (IOException ex) {
            // Controla posible fallo al guardar los perfiles de usuario (posiblemente un problema de permisos)
            System.out.println("[FAIL] Error al escribir el archivo de perfiles");
        }
    }

    public static void importProfile() {
        new Thread(() -> {
            JFileChooser selector = new JFileChooser();
            selector.setDialogTitle("Importar perfil");
            selector.setApproveButtonText("Importar");
            selector.setApproveButtonToolTipText("Importa el perfil seleccionado");
            FileNameExtensionFilter bbpFilter = new FileNameExtensionFilter("Bob Penguins Profile (*.bpp)", "bpp");
            selector.addChoosableFileFilter(bbpFilter);
            selector.setFileFilter(bbpFilter);
            int option = selector.showOpenDialog(null);
            if (option == JFileChooser.APPROVE_OPTION) {
                try {
                    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(selector.getSelectedFile().getPath()));
                    Profile profile = (Profile) ois.readObject();
                    ProfilesScreen.profiles.add(profile);
                    ois.close();
                    System.out.println("[ OK ] Perfil \"" + profile.getName() + "\" importado con exito!");
                } catch (Exception ex) {
                    // Controla cualquier error de lectura del fichero
                    System.out.println("[FAIL] Error al importar el perfil");
                }
            }
        }).start();
    }

    public static void exportProfile(Profile profile) {
        new Thread(() -> {
            JFileChooser selector = new JFileChooser();
            selector.setDialogTitle("Exportar perfil");
            selector.setSelectedFile(new File(profile.getName() + ".bpp"));
            FileNameExtensionFilter bbpFilter = new FileNameExtensionFilter("Bob Penguins Profile (*.bpp)", "bpp");
            selector.addChoosableFileFilter(bbpFilter);
            selector.setFileFilter(bbpFilter);
            selector.setApproveButtonText("Exportar");
            selector.setApproveButtonToolTipText("Exporta el perfil seleccionado");
            int option = selector.showSaveDialog(null);
            if (option == JFileChooser.APPROVE_OPTION) {
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(selector.getSelectedFile().getPath()));
                    oos.writeObject(profile);
                    oos.close();
                    System.out.println("[ OK ] Perfil \"" + profile.getName() + "\" exportado con exito!");
                } catch (Exception ex) {
                    // Controla posible fallo al exportar el perfil de usuario (Posiblemente un problema de permisos)
                    System.out.println("[FAIL] Error al exportar el perfil");
                }
            }
        }).start();
    }
}
