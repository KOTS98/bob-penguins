package apalacios.pfg.util;

public class GameScore {

    // Conteo de rondas
    public static int totalRounds;
    public static int currentRound;
    public static int lastLevel;
    public static int nPlayers;

    // Puntuación
    public static int scoreP1;
    public static int scoreP2;
    public static int scoreP3;
    public static int scoreP4;
}
