package apalacios.pfg.weapons;

import apalacios.pfg.characters.Proyectile;
import apalacios.pfg.characters.Weapon;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.Direction;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Weapon8 extends Weapon {
    private double lastShot;
    private boolean charging;
    private boolean hasShot;

    private float charge;
    private Sound chargingSound;
    private Sound dechargingSound;

    public Weapon8(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 8, -1);
        nMunition = 1;

        shootSpeed = 2000;
        lastShot = System.currentTimeMillis() - shootSpeed;
        charging = false;
        charge = 0;
        hasShot = false;

        shootSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/disparoBFG.ogg"));
        chargingSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/cargaBFG.ogg"));
        dechargingSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/descargaBFG.ogg"));
    }

    @Override
    public void shoot() {
        double currentShot = System.currentTimeMillis();
        if (lastShot - currentShot < -shootSpeed && !charging) {
            lastShot = currentShot;
            charging = true;
            chargingSound.play(Config.soundVolume);
        }
    }

    private void releaseCharge() {
        Proyectile pr;
        shootSound.play(Config.soundVolume);
        if (direction == Direction.RIGHT)
            pr = new Proyectile(getX() + (getWidth() /2) + 40, getY() + 30, getStage(), 1000, direction,
                    false, 8, 2000);
        else
            pr = new Proyectile(getX() + (getWidth() /2) - 70, getY() + 30, getStage(), 1000, direction,
                    false, 8, 2000);
        pr.addAction(Actions.forever(Actions.rotateBy(360, 0.2f)));
    }

    @Override
    public void stopShooting() {
        chargingSound.stop();
        if (hasShot)
            hasShot = false;
        else
            dechargingSound.play(Config.soundVolume);
        charging = false;
    }

    @Override
    public void reload() {

    }

    @Override
    public void act(float dt) {
        super.act(dt);
        if (charging) {
            charge += dt;
        } else {
            if (charge > 0)
                charge -= dt;
            else
                charge = 0;
        }

        if (charge >= 1.5f) {
            charging = false;
            charge = 0;
            hasShot = true;
            releaseCharge();
        }
    }
}
