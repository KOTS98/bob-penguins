package apalacios.pfg.screens;

import apalacios.pfg.characters.*;
import apalacios.pfg.controllers.ControllerProfile;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.game.PFGGame;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.DeadType;
import apalacios.pfg.util.Profile;
import apalacios.pfg.util.TiledMapActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.ArrayList;

public class TestControllerScreen extends BaseGamepadScreen {
    private Controller controller;
    private Profile profile;
    private Controller currentController;
    private ControllerProfile controllerProfile;

    private TiledMapActor tma;

    private Player player;
    private float deadTime;
    private boolean playerIsDead;

    TestControllerScreen(Profile profile, Controller controller) {
        this.controller = controller;
        this.profile = profile;

        detectController();

        new Thread(() -> StartMenuScreen.titleMusic.setOnCompletionListener(music -> {
            if (StartMenuScreen.currentSong == 13)
                StartMenuScreen.currentSong = 1;
            else
                StartMenuScreen.currentSong++;

            String songName = "04 Shell Shock Shake";
            switch (StartMenuScreen.currentSong) {
                case 1:
                    songName = "01 Introjuice";
                    break;
                case 2:
                    songName = "02 Failien Funk";
                    break;
                case 3:
                    songName = "03 Stroll 'n Roll";
                    break;
                case 4:
                    songName = "04 Shell Shock Shake";
                    break;
                case 5:
                    songName = "05 I'm a Fighter";
                    break;
                case 6:
                    songName = "06 Going Down Tune";
                    break;
                case 7:
                    songName = "07 Cloud Crash";
                    break;
                case 8:
                    songName = "08 Filaments and Voids";
                    break;
                case 9:
                    songName = "09 Bonus Rage";
                    break;
                case 10:
                    songName = "10 It's not My Ship";
                    break;
                case 11:
                    songName = "11 Perihelium";
                    break;
                case 12:
                    songName = "12 Shingle Tingle";
                    break;
                case 13:
                    songName = "13 Just a Minuet";
                    break;
            }

            StartMenuScreen.titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                    ("src/apalacios/pfg/assets/music/" + songName + ".mp3"));
            StartMenuScreen.titleMusic.setLooping(false);
            StartMenuScreen.titleMusic.play();
            StartMenuScreen.titleMusic.setVolume(Config.musicVolume);
        })).start();

        Label startSalirLabel = new Label("PULSA START / ESC PARA SALIR", BaseGame.styleNormal);
        startSalirLabel.setPosition(20, 1060 - startSalirLabel.getHeight());
        uiStage.addActor(startSalirLabel);
        startSalirLabel.addAction(Actions.forever(Actions.sequence(Actions.fadeOut(1.5f), Actions.fadeIn(1))));

        deadTime = 0;
        playerIsDead = false;
    }

    private void detectController() {

        ArrayList<ControllerProfile> controllerProfiles = profile.getControllerProfiles();
        this.currentController = controller;

        try {
            for (ControllerProfile cp : controllerProfiles) {
                if (cp.getControllerType().equalsIgnoreCase(currentController.getName())) {
                    controllerProfile = cp;
                    break;
                }
            }
        } catch (NullPointerException ex) {
            // Controla la posibilidad de que se haya entrado a la pantalla por teclado y no se detecte ningún mando
        }
        if (controllerProfile == null)
            controllerProfile = createDefaultControllerProfile();
    }

    private ControllerProfile createDefaultControllerProfile() {
        ControllerProfile cp = new ControllerProfile();
        cp.setJoystickX(1);
        cp.setJoystickY(0);
        cp.setButtonJump(1);
        cp.setButtonShoot(2);
        cp.setButtonGrab(0);
        cp.setButtonThrow(3);
        cp.setButtonStart(7);
        return cp;
    }

    @Override
    public void initialize() {

        BaseActor background = new BaseActor(0, 0, mainStage);
        background.loadTexture("src/apalacios/pfg/assets/background/nieve.png");

        tma = new TiledMapActor("src/apalacios/pfg/assets/map/test_map_bg.tmx", mainStage);

        MapObject startPoint = tma.getRectangleList("Start").get(0);
        MapProperties startProps = startPoint.getProperties();
        player = new Player((float)startProps.get("x"), (float)startProps.get("y") + 200, mainStage, 1,
                0, false);

        for(MapObject obj: tma.getRectangleList("Solid")) {
            MapProperties props = obj.getProperties();
            new Solid((float)props.get("x"), (float)props.get("y"), (float)props.get("width"),
                    (float)props.get("height"), mainStage);
        }

        for(MapObject obj: tma.getRectangleList("Platform")) {
            MapProperties props = obj.getProperties();
            new Platform((float)props.get("x"), (float)props.get("y"), (float)props.get("width"),
                    (float)props.get("height"), mainStage);
        }

        for(MapObject obj: tma.getRectangleList("Crate")) {
            MapProperties props = obj.getProperties();
            new Crate((float)props.get("x"), (float)props.get("y"), 36,
                    36, mainStage);
        }

        for(MapObject obj: tma.getRectangleList("weapon")) {
            MapProperties props = obj.getProperties();
            new WeaponItem((float)props.get("x"), (float)props.get("y") + 20, mainStage, 2, -1);
        }

        for(MapObject obj: tma.getRectangleList("Trampoline")) {
            MapProperties props = obj.getProperties();
            new Trampoline((float)props.get("x"), (float)props.get("y"), mainStage);
        }
    }

    @Override
    public boolean povMoved(Controller controller, int i, PovDirection povDirection) {
        if (this.controller == null) {
            this.controller = controller;
            detectController();
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int i, float v) {
        if (this.controller == null) {
            this.controller = controller;
            detectController();
        }
        if (controller == currentController) {
            if (i == controllerProfile.getJoystickX()) {
                if (v == 1) {
                    player.moveRight();
                } else if(v == -1) {
                    player.moveLeft();
                } else {
                    player.decelerate();
                }
            }
            if (i == controllerProfile.getJoystickY()) {
                if (v == 1) {
                    if (player.isOnPlatform())
                        player.goDown(true);
                } else {
                    player.goDown(false);
                }
            }
        }
       return false;
    }

    @Override
    public boolean buttonDown(Controller controller, int i) {
        if (this.controller == null) {
            this.controller = controller;
            detectController();
        }
        if (controller == currentController) {
            if (i == controllerProfile.getButtonJump()) {
                player.jump();
                player.setOnGround(false);
            }
            if (i == controllerProfile.getButtonShoot())
                player.shoot();
            if (i == controllerProfile.getButtonGrab())
                player.grabItem();
            if (i == controllerProfile.getButtonThrow())
                player.throwItem();
            if (i == controllerProfile.getButtonStart())
                PFGGame.setActiveScreen(new ProfilesScreen());
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int i) {
        if (this.controller == null) {
            this.controller = controller;
            detectController();
        }
        if (controller == currentController) {
            if (i == controllerProfile.getButtonShoot()) {
                player.stopShooting();
            }
        }
        return false;
    }

    @Override
    public void update(float dt) {
        for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
            if (mobileActor.getName().equalsIgnoreCase("player")) {
                Player playerActor = (Player) mobileActor;
                playerActor.setOnGround(false);
                playerActor.setOnPlatform(false);
            }
        }

        // COLISION MOVIL -> SUELO
        for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
            for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
                if (mobileActor.getName().equalsIgnoreCase("player")) {
                    Player playerActor = (Player) mobileActor;
                    if (playerActor.isAlive()) {
                        if (playerActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                            if (playerActor.getFeetSensorL().overlaps(solidActor) && playerActor.getFeetSensorR().overlaps(solidActor)) {
                                playerActor.setPosition(playerActor.getX(), solidActor.getY() + solidActor.getHeight() + 2);
                            }
                        }

                        if (playerActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                            if (playerActor.getFeetSensorL().overlaps(solidActor) || playerActor.getFeetSensorR().overlaps(solidActor)) {
                                playerActor.setGravityEnabled(false);
                                playerActor.setOnGround(true);
                            }
                        } else {
                            playerActor.preventOverlap(solidActor);
                        }
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("crate")) {
                    Crate crateActor = (Crate) mobileActor;
                    if (crateActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                        if (crateActor.getFeetSensor().overlaps(solidActor)) {
                            crateActor.setPosition(crateActor.getX(), solidActor.getY() + solidActor.getHeight() + 1);
                        }
                    }

                    if (crateActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                        if (crateActor.getFeetSensor().overlaps(solidActor)) {
                            crateActor.setGravityEnabled(false);
                        }
                    } else {
                        crateActor.preventOverlap(solidActor);
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("weapon")) {
                    WeaponItem weaponItemActor = (WeaponItem) mobileActor;
                    if (weaponItemActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                        if (weaponItemActor.getFeetSensor().overlaps(solidActor)) {
                            weaponItemActor.setPosition(weaponItemActor.getX(), solidActor.getY() + solidActor.getHeight() + 1);
                        }
                    }

                    if (weaponItemActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                        if (weaponItemActor.getFeetSensor().overlaps(solidActor)) {
                            weaponItemActor.setGravityEnabled(false);
                        }
                    } else {
                        weaponItemActor.preventOverlap(solidActor);
                    }
                }
            }
        }

        // COLISION MOVIL -> PLATAFORMA
        for (BaseActor platformActor : BaseActor.getList(mainStage, Platform.class.getName())) {
            for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
                if (mobileActor.getName().equalsIgnoreCase("player")) {
                    Player playerActor = (Player) mobileActor;
                    if (playerActor.isAlive()) {
                        if (playerActor.getVelocityVector().y <= 0 && playerActor.isGravityLock()) {
                            if (playerActor.getY() > platformActor.getY() + platformActor.getHeight() - 10 &&
                                    playerActor.getY() <= platformActor.getY() + platformActor.getHeight()) {
                                if (playerActor.getFeetSensorL().overlaps(platformActor) ||
                                        playerActor.getFeetSensorR().overlaps(platformActor)) {
                                    playerActor.setPosition(playerActor.getX(), platformActor.getY() +
                                            platformActor.getHeight() + 2);
                                }
                            }

                            if (playerActor.getY() > platformActor.getY() + platformActor.getHeight()) {
                                if (playerActor.getFeetSensorL().overlaps(platformActor) ||
                                        playerActor.getFeetSensorR().overlaps(platformActor)) {
                                    playerActor.setGravityEnabled(false);
                                    playerActor.setOnGround(true);
                                    playerActor.setOnPlatform(true);
                                }
                            }
                        }
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("crate")) {
                    Crate crateActor = (Crate) mobileActor;
                    if (crateActor.getVelocityVector().y <= 0) {
                        if (crateActor.getY() > platformActor.getY() + platformActor.getHeight() - 10 &&
                                crateActor.getY() <= platformActor.getY() + platformActor.getHeight()) {
                            if (crateActor.getFeetSensor().overlaps(platformActor)) {
                                crateActor.setPosition(crateActor.getX(), platformActor.getY() +
                                        platformActor.getHeight() + 2);
                            }
                        }

                        if (crateActor.getY() > platformActor.getY() + platformActor.getHeight()) {
                            if (crateActor.getFeetSensor().overlaps(platformActor)) {
                                crateActor.setGravityEnabled(false);
                            }
                        }
                    }
                }
            }
        }

        // COLISION JUGADOR -> MOVIL
        if (player.isAlive()) {
            for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
                if (mobileActor.getName().equalsIgnoreCase("Crate")) {
                    Crate crate = (Crate) mobileActor;
                    if (player.isKickingNow()) {
                        if (player.getKickBox().overlaps(crate)) {
                            crate.reciveKick(player.getDirection());
                        }
                    }

                    if (player.isWantsToGrab()) {
                        if (player.overlaps(crate.getGrabSensor())) {
                            crate.remove();
                            player.setWeapon(0);
                            break;
                        }
                    }

                    if (crate.getFeetSensor().overlaps(player) && crate.getVelocityVector().y < 0) {
                        crate.destroy();
                        player.die(DeadType.CRUSHED);
                        playerIsDead = true;
                        break;
                    }

                    if (player.getY() <= crate.getY() + crate.getHeight()) {
                        if (player.getFeetSensorL().overlaps(crate) && player.getFeetSensorR().overlaps(crate)) {
                            player.setPosition(player.getX(), crate.getY() + crate.getHeight() + 2);
                        }
                    }

                    if (player.getY() > crate.getY() + crate.getHeight()) {
                        if (player.getFeetSensorL().overlaps(crate) || player.getFeetSensorR().overlaps(crate)) {
                            player.setGravityEnabled(false);
                            player.setOnGround(true);
                        }
                    } else {
                        if (player.overlaps(crate)) {
                            crate.move(player.getVelocityVector().x / 2, 0);
                            player.preventOverlap(crate);
                        }
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("weapon")) {
                    WeaponItem weaponItemActor = (WeaponItem) mobileActor;
                    if (player.isWantsToGrab()) {
                        if (player.overlaps(weaponItemActor.getGrabSensor()) && player.getWeapon() == null) {
                            player.setWeapon(weaponItemActor.getNWeapon());
                            weaponItemActor.remove();
                        }
                    }
                }
            }
        }

        // COLISION MOVIL CON TRAMPOLIN
        for (BaseActor trampolineActor : BaseActor.getList(mainStage, Trampoline.class.getName())) {
            Trampoline trampoline = (Trampoline) trampolineActor;
            for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
                if (mobileActor.getName().equalsIgnoreCase("Player")) {
                    if (player.getFeetSensorR().overlaps(trampoline) || player.getFeetSensorL().overlaps(trampoline))
                        trampoline.jump(player);
                    else if (player.overlaps(trampoline))
                        player.preventOverlap(trampoline);
                } else if (mobileActor.getName().equalsIgnoreCase("Crate")) {
                    Crate crate = (Crate) mobileActor;
                    if (crate.overlaps(trampoline)) {
                        trampoline.jump(crate);
                    }
                } else if (mobileActor.getName().equalsIgnoreCase("Weapon")) {
                    WeaponItem weaponItem = (WeaponItem) mobileActor;
                    if (weaponItem.overlaps(trampoline)) {
                        trampoline.jump(weaponItem);
                    }
                }
            }
        }

        // COLISION PROYECTILES -> JUGADOR, CAJAS Y MUROS
        for (BaseActor proyectileActor : BaseActor.getList(mainStage, Proyectile.class.getName())) {
            Proyectile pr = (Proyectile) proyectileActor;
            for (BaseActor playerActor : BaseActor.getList(mainStage, Player.class.getName())) {
                Player player = (Player) playerActor;
                if (proyectileActor.overlaps(player) && player.isAlive()) {
                    player.die(DeadType.SHOT);
                    if (pr.getnProyectile() != 8)
                        proyectileActor.remove();
                }
            }
            for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
                if (proyectileActor.overlaps(solidActor)) {
                    proyectileActor.remove();
                }
            }
            for (BaseActor crateActor : BaseActor.getList(mainStage, Crate.class.getName())) {
                if (proyectileActor.overlaps(crateActor)) {
                    Crate crate = (Crate) crateActor;
                    proyectileActor.remove();
                    crate.destroy();
                }
            }
        }

        // ACTIVAR GRAVEDAD
        for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
            if (mobileActor.getName().equalsIgnoreCase("player")) {
                Player playerActor = (Player) mobileActor;
                if (!playerActor.isOnPlatform() && !playerActor.isOnGround()) {
                    playerActor.setGravityEnabled(true);
                }
            } else if (mobileActor.getName().equalsIgnoreCase("crate")) {
                Crate crateActor = (Crate) mobileActor;
                boolean feetColision = false;
                for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
                    if (crateActor.getFeetSensor().overlaps(solidActor))
                        feetColision = true;
                }
                for (BaseActor platformActor : BaseActor.getList(mainStage, Platform.class.getName())) {
                    if (crateActor.getFeetSensor().overlaps(platformActor))
                        feetColision = true;
                }
                if (!feetColision)
                    crateActor.setGravityEnabled(true);
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            PFGGame.setActiveScreen(new ProfilesScreen());
        }

        if (playerIsDead) {
            deadTime += dt;
            if (deadTime >= 2) {
                MapObject startPoint = tma.getRectangleList("Start").get(0);
                MapProperties startProps = startPoint.getProperties();
                player = new Player((float) startProps.get("x"), (float) startProps.get("y") + 200, mainStage, 1,
                        0, false);
                playerIsDead = false;
                deadTime = 0;
            }
        }

        if (player.getY() < 50) {
            MapObject startPoint = tma.getRectangleList("Start").get(0);
            MapProperties startProps = startPoint.getProperties();
            player.setPosition((float)startProps.get("x"), (float)startProps.get("y"));
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
        mainStage.getViewport().update(arg0, arg1, true);
    }
}
