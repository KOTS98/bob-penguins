package apalacios.pfg.game;

import apalacios.pfg.screens.StartMenuScreen;

public class PFGGame extends BaseGame {

    @Override
    public void create() {
        super.create();
        setActiveScreen(new StartMenuScreen());
    }
}
