package apalacios.pfg.screens;

import apalacios.pfg.characters.BaseActor;
import apalacios.pfg.controllers.ControllerProfile;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.game.PFGGame;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.Profile;
import apalacios.pfg.util.Util;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.ArrayList;

public class AddControllerScreen extends BaseGamepadScreen {
    private long startInteractionTime;
    private ControllerProfile controllerProfile;
    private Controller controllerInUse;
    private ArrayList<Profile> profiles;
    private Profile currentProfile;

    private enum State {
        SELECT, MOVE_R, MOVE_L, MOVE_D, JUMP, GRAB, FIRE, THROW, START
    }

    private State state;

    private BaseActor tvImage;

    // Seleccionar mando
    private Label pulsaBtnMandoLabel;

    // Mover a la derecha
    private Label caminaDerechaLabel;

    // Mover a la izquierda
    private Label caminaIzquierdaLabel;

    // Mover hacia abajo
    private Label dejarCaerLabel;

    // Saltar
    private Label saltarLabel;

    // Coger
    private Label cogerArmaLabel;

    // Disparar
    private Label dispararLabel;

    // Lanzar
    private Label lanzarLabel;

    // Start
    private Label startLabel;

    // Elemetos del mapa
    private BaseActor player;
    private BaseActor wing;
    private BaseActor enemy;
    private BaseActor enemyWing;
    private BaseActor weapon;
    private BaseActor enemyWeapon;

    AddControllerScreen(Profile currentProfile) {
        profiles = Util.loadProfiles();
        this.currentProfile = currentProfile;
    }

    @Override
    public void initialize() {
        startInteractionTime = System.currentTimeMillis();
        state = State.SELECT;

        controllerProfile = new ControllerProfile();

        // Televisión
        BaseActor background = new BaseActor(0, 0, mainStage);
        background.setAnimation(background.loadTexture("src/apalacios/pfg/assets/background/add_mando_fondo.png"));
        tvImage = new BaseActor(0, 0, mainStage);
        tvImage.setAnimation(tvImage.loadTexture("src/apalacios/pfg/assets/background/tv_mando_select.png"));

        // Seleccionar mando
        pulsaBtnMandoLabel = new Label("PULSA UN BOTON EN EL MANDO\n" +
                "  QUE QUIERAS CONFIGURAR", BaseGame.styleBig);
        pulsaBtnMandoLabel.setPosition(450, -50);
        uiStage.addActor(pulsaBtnMandoLabel);
        pulsaBtnMandoLabel.addAction(Actions.moveTo(450, 800, 0.2f));

        // Mover a la derecha
        caminaDerechaLabel = new Label("CAMINA HACIA LA DERECHA", BaseGame.styleBig);
        caminaDerechaLabel.setPosition(500, -50);
        uiStage.addActor(caminaDerechaLabel);

        // Mover a la izquierda
        caminaIzquierdaLabel = new Label("CAMINA HACIA LA IZQUIERDA", BaseGame.styleBig);
        caminaIzquierdaLabel.setPosition(475, -50);
        uiStage.addActor(caminaIzquierdaLabel);

        // Moverse hacia abajo
        dejarCaerLabel = new Label("BAJA DE LA PLATAFORMA", BaseGame.styleBig);
        dejarCaerLabel.setPosition(525, -50);
        uiStage.addActor(dejarCaerLabel);

        // Saltar
        saltarLabel = new Label("DA UN SALTO", BaseGame.styleBig);
        saltarLabel.setPosition(750, -50);
        uiStage.addActor(saltarLabel);

        // Coger
        cogerArmaLabel = new Label("COGE EL ARMA", BaseGame.styleBig);
        cogerArmaLabel.setPosition(735, -50);
        uiStage.addActor(cogerArmaLabel);

        // Disparar
        dispararLabel = new Label("DISPARA EL ARMA", BaseGame.styleBig);
        dispararLabel.setPosition(680, -50);
        uiStage.addActor(dispararLabel);

        // Lanzar
        lanzarLabel = new Label("SUELTA EL ARMA", BaseGame.styleBig);
        lanzarLabel.setPosition(650, -50);
        uiStage.addActor(lanzarLabel);

        // Start
        startLabel = new Label("PULSA START PARA TERMINAR", BaseGame.styleBig);
        startLabel.setPosition(475, -50);
        uiStage.addActor(startLabel);

        // Marco pantalla
        // Marco pantalla
        BaseActor screenFrame = new BaseActor(0, 0, uiStage);
        screenFrame.loadTexture("src/apalacios/pfg/assets/background/screenFrame.png");

        // Música
        new Thread(() -> StartMenuScreen.titleMusic.setOnCompletionListener(music -> {
            if (StartMenuScreen.currentSong == 13)
                StartMenuScreen.currentSong = 1;
            else
                StartMenuScreen.currentSong++;

            String songName = "04 Shell Shock Shake";
            switch (StartMenuScreen.currentSong) {
                case 1:
                    songName = "01 Introjuice";
                    break;
                case 2:
                    songName = "02 Failien Funk";
                    break;
                case 3:
                    songName = "03 Stroll 'n Roll";
                    break;
                case 4:
                    songName = "04 Shell Shock Shake";
                    break;
                case 5:
                    songName = "05 I'm a Fighter";
                    break;
                case 6:
                    songName = "06 Going Down Tune";
                    break;
                case 7:
                    songName = "07 Cloud Crash";
                    break;
                case 8:
                    songName = "08 Filaments and Voids";
                    break;
                case 9:
                    songName = "09 Bonus Rage";
                    break;
                case 10:
                    songName = "10 It's not My Ship";
                    break;
                case 11:
                    songName = "11 Perihelium";
                    break;
                case 12:
                    songName = "12 Shingle Tingle";
                    break;
                case 13:
                    songName = "13 Just a Minuet";
                    break;
            }

            StartMenuScreen.titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                    ("src/apalacios/pfg/assets/music/" + songName + ".mp3"));
            StartMenuScreen.titleMusic.setLooping(false);
            StartMenuScreen.titleMusic.play();
            StartMenuScreen.titleMusic.setVolume(Config.musicVolume);
        })).start();
    }

    @Override
    public boolean axisMoved(Controller controller, int i, float v) {
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < - 1000 && (v == 1 || v == -1)) {
            startInteractionTime = currentInteractionTime;
            if (controller == controllerInUse) {
                if (state == State.MOVE_R) {
                    controllerProfile.setJoystickX(i);
                    state = State.MOVE_L;
                    caminaDerechaLabel.addAction(Actions.moveTo(1930, 820, 0.2f));
                    caminaIzquierdaLabel.addAction(Actions.moveTo(475, 820, 0.2f));
                    player.setAnimation(player.loadAnimationFromSheet("src/apalacios/pfg/assets/characters" +
                                    "/pj1/pj1_walk_right.png", 1, 8, 0.1f,
                            true));
                    player.addAction(Actions.moveTo(1100, 465, 1));
                } else if (state == State.MOVE_L) {
                    state = State.MOVE_D;
                    caminaIzquierdaLabel.addAction(Actions.moveTo(1930, 820, 0.2f));
                    dejarCaerLabel.addAction(Actions.moveTo(525, 820, 0.2f));
                    player.setAnimation(player.loadAnimationFromSheet("src/apalacios/pfg/assets/characters" +
                                    "/pj1/pj1_walk_left.png", 1, 8, 0.1f,
                            true));
                    wing.setAnimation(wing.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_left.png"));
                    player.addAction(Actions.moveTo(900, 465, 1));
                } else if (state == State.MOVE_D && i != controllerProfile.getJoystickX()) {
                    controllerProfile.setJoystickY(i);
                    state = State.JUMP;
                    dejarCaerLabel.addAction(Actions.moveTo(1930, 820, 0.2f));
                    saltarLabel.addAction(Actions.moveTo(750, 820, 0.2f));
                    player.setAnimation(player.loadTexture("src/apalacios/pfg/assets/characters/pj1/" +
                            "pj1_stand_right.png"));
                    wing.setAnimation(wing.loadTexture("src/apalacios/pfg/assets/characters/pj1/" +
                            "aleta_1_right.png"));
                    player.addAction(Actions.moveTo(900, 260, 0.5f));
                }
            }
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int i) {
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < - 1000) {
            startInteractionTime = currentInteractionTime;
            if (state == State.SELECT) {
                controllerInUse = controller;
                controllerProfile.setControllerType(controller.getName());
                state = State.MOVE_R;
                tvImage.setAnimation(tvImage.loadTexture("src/apalacios/pfg/assets/background/tv_mando_conf.png"));
                pulsaBtnMandoLabel.addAction(Actions.moveTo(1930, 800, 0.2f));
                caminaDerechaLabel.addAction(Actions.moveTo(500, 820, 0.2f));

                // Elementos del mapa
                player = new BaseActor(900, 465, mainStage);
                player.setAnimation(player.loadTexture("src/apalacios/pfg/assets/characters/pj1/pj1_stand_right.png"));
                wing = new BaseActor(player.getX(), player.getY(), player.getStage());
                wing.setAnimation(wing.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_right.png"));

                enemy = new BaseActor(1280, 260, mainStage);
                enemy.setAnimation(enemy.loadTexture("src/apalacios/pfg/assets/characters/pj6/pj6_stand_left.png"));
                enemyWing = new BaseActor(enemy.getX(), enemy.getY(), enemy.getStage());
                enemyWing.setAnimation(enemyWing.loadTexture("src/apalacios/pfg/assets/characters/pj6/" +
                        "aleta_6_alt_left.png"));

                weapon = new BaseActor(920, 260, mainStage);
                weapon.setAnimation(weapon.loadTexture("src/apalacios/pfg/assets/weapons/ar3/right_item.png"));

                enemyWeapon = new BaseActor(enemy.getX(), enemy.getY(), enemy.getStage());
                enemyWeapon.setAnimation(enemyWeapon.loadTexture("src/apalacios/pfg/assets/weapons/ar3/left.png"));

                // Filtro televisión
                BaseActor tvFilter = new BaseActor(0, 0, mainStage);
                tvFilter.setAnimation(tvFilter.loadTexture("src/apalacios/pfg/assets/background/filtro_pantalla.png"));

            } else if (state == State.JUMP && controller == controllerInUse) {
                controllerProfile.setButtonJump(i);
                state = State.GRAB;
                saltarLabel.addAction(Actions.moveTo(1930, 820, 0.2f));
                cogerArmaLabel.addAction(Actions.moveTo(735, 820, 0.2f));

                player.addAction(Actions.delay(0.1f));
                player.addAction(Actions.after(Actions.moveTo(player.getX(), 360, 0.1f)));
                player.addAction(Actions.after(Actions.moveTo(player.getX(),  410, 0.2f)));
                player.addAction(Actions.after(Actions.moveTo(player.getX(), 360, 0.2f)));
                player.addAction(Actions.after(Actions.moveTo(player.getX(), 260, 0.1f)));

                BaseActor enemyProyectile = new BaseActor(enemyWeapon.getX(), enemyWeapon.getY() + 48,
                        enemyWeapon.getStage());
                enemyProyectile.setAnimation(enemyProyectile.loadTexture("src/apalacios/pfg/assets/proyectiles" +
                        "/pr1/left.png"));
                enemyProyectile.addAction(Actions.moveTo(280, enemyProyectile.getY(), 1));
                enemyProyectile.addAction(Actions.after(Actions.removeActor()));
            } else if (state == State.GRAB && controller == controllerInUse && i != controllerProfile.getButtonJump()) {
                controllerProfile.setButtonGrab(i);
                state = State.FIRE;
                cogerArmaLabel.addAction(Actions.moveTo(1930, 820, 0.2f));
                dispararLabel.addAction(Actions.moveTo(680, 820, 0.2f));
                weapon.setAnimation(weapon.loadTexture("src/apalacios/pfg/assets/weapons/ar3/right.png"));
                weapon.centerAtActor(player);
                wing.setAnimation(wing.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_alt_right.png"));
            } else if (state == State.FIRE && controller == controllerInUse && i != controllerProfile.getButtonJump() &&
            i != controllerProfile.getButtonGrab()) {
                controllerProfile.setButtonShoot(i);
                state = State.THROW;
                dispararLabel.addAction(Actions.moveTo(1930, 820, 0.2f));
                lanzarLabel.addAction(Actions.moveTo(650, 820, 0.2f));
                BaseActor proyectile = new BaseActor(weapon.getX() + 45, weapon.getY() + 48, weapon.getStage());
                proyectile.setAnimation(proyectile.loadTexture("src/apalacios/pfg/assets/proyectiles/pr1/" +
                        "right.png"));
                proyectile.addAction(Actions.moveTo(enemy.getX(), proyectile.getY(), 0.4f));
                proyectile.addAction(Actions.after(Actions.removeActor()));
                enemy.addAction(Actions.delay(0.4f));
                enemy.addAction(Actions.after(Actions.fadeOut(1)));
                enemyWing.addAction(Actions.delay(0.4f));
                enemyWing.addAction(Actions.after(Actions.fadeOut(1)));
                enemyWeapon.addAction(Actions.delay(0.4f));
                enemyWeapon.addAction(Actions.after(Actions.fadeOut(1)));

            } else if (state == State.THROW && controller == controllerInUse && i != controllerProfile.getButtonJump() &&
            i != controllerProfile.getButtonGrab() && i != controllerProfile.getButtonShoot()) {
                controllerProfile.setButtonThrow(i);
                state = State.START;
                lanzarLabel.addAction(Actions.moveTo(1930, 820, 0.2f));
                startLabel.addAction(Actions.moveTo(475, 820, 0.2f));
                weapon.setAnimation(weapon.loadTexture("src/apalacios/pfg/assets/weapons/ar3/right_item.png"));
                weapon.addAction(Actions.moveTo(weapon.getX() + 100, weapon.getY() + 50, 0.2f));
                weapon.addAction(Actions.delay(0.2f));
                weapon.addAction(Actions.after(Actions.moveTo(weapon.getX() + 200, weapon.getY(), 0.2f)));
                wing.setAnimation(wing.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_right.png"));
            } else if (state == State.START && controller == controllerInUse && i != controllerProfile.getButtonJump() &&
                    i != controllerProfile.getButtonGrab() && i != controllerProfile.getButtonShoot() &&
                    i != controllerProfile.getButtonThrow()) {
                controllerProfile.setButtonStart(i);
                startLabel.addAction(Actions.moveTo(1930, 820, 0.2f));
                saveProfileConfiguration();
                PFGGame.setActiveScreen(new ProfilesScreen());
            }
        }
        return false;
    }

    @Override
    public void update(float dt) {
        if (state != State.SELECT) {
            wing.centerAtActor(player);
        }
    }

    private void saveProfileConfiguration() {
        for (Profile profile : profiles) {
            if (profile.getName().equalsIgnoreCase(currentProfile.getName())) {
                profile.addControllerProfile(controllerProfile);
            }
        }
        Util.saveProfiles(profiles);
    }

    @Override
    public void resize(int arg0, int arg1) {
        mainStage.getViewport().update(arg0, arg1, true);
    }
}
