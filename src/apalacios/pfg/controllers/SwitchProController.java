package apalacios.pfg.controllers;

import com.badlogic.gdx.controllers.PovDirection;

public class SwitchProController {
    public static final int BUTTON_A = 2;
    public static final int BUTTON_B = 1;
    public static final int BUTTON_X = 3;
    public static final int BUTTON_Y = 0;
    public static final int BUTTON_LEFT_SHOULDER = 4;
    public static final int BUTTON_RIGHT_SHOULDER = 5;
    public static final int BUTTON_BACK = 8;
    public static final int BUTTON_START = 9;
    public static final int BUTTON_LEFT_STICK = 10;
    public static final int BUTTON_RIGHT_STICK = 11;

    // Códigos para el pad direccional (cruceta)
    public static final PovDirection DPAD_UP = PovDirection.north;
    public static final PovDirection DPAD_DOWN = PovDirection.south;
    public static final PovDirection DPAD_RIGHT = PovDirection.east;
    public static final PovDirection DPAD_LEFT = PovDirection.west;

    // Códigos para el joystick
    // Eje X: -1 = izquierda, +1 = derecha
    // Eje Y: -1 = arriba, +1 = abajo
    public static final int AXIS_LEFT_X = 2;
    public static final int AXIS_LEFT_Y = 3;
    public static final int AXIS_RIGHT_X = 0;
    public static final int AXIS_RIGHT_Y = 1;

    // Códigos para los gatillos
    public static final int AXIS_LEFT_TRIGGER = 6;
    public static final int AXIS_RIGHT_TRIGGER = 7;
}
