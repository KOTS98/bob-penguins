package apalacios.pfg.characters;

import apalacios.pfg.util.Direction;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Crate extends Mobile {
    private float posX;
    private float posY;
    private float width;
    private float height;
    private boolean alive;
    private BaseActor crateDestroyed;

    // Físicas
    private float accelerationX;
    private float accelerationY;
    private float maxHorizontalSpeed;
    private float horizontalDeceleration;
    private float gravity;
    private boolean gravityEnabled;
    private float maxFallSpeed;

    private boolean moving;
    private Direction direction;
    private BaseActor feetSensor;
    private BaseActor grabSensor;

    // Visual
    private boolean bigCrate;

    /**
     * Constructor de la calse, inicializar los parámetros de la misma
     *
     * @param posX  Representa su posición respecto a la pantalla en el eje horizontal
     * @param posY  Representa su posición respecto a la pantalla en el eje vertical
     * @param stage El actor (independientemente de lo que sea), sera añadido al stage,
     */
    public Crate(float posX, float posY, float width, float height, Stage stage) {
        super(posX, posY, stage);
        setName("crate");

        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;

        setAnimation(loadTexture("src/apalacios/pfg/assets/world_elements/caja_stand.png"));
        setBoundaryRectangle();

        alive = true;
        crateDestroyed = new BaseActor(getX(), getY(), getStage());

        // Físicas
        horizontalDeceleration = 2;
        maxHorizontalSpeed = 2000;
        gravity = 5;
        gravityEnabled = true;
        maxFallSpeed = 800;

        moving = false;
        direction = Direction.RIGHT;
        feetSensor = new BaseActor(getX(), getY(), getStage());
        feetSensor.setSize(11, 5);
        feetSensor.setBoundaryRectangle();

        grabSensor = new BaseActor(getX(), getY(), getStage());
        grabSensor.setSize(55, 40);
        grabSensor.setBoundaryRectangle();

        bigCrate = false;
    }

    public float getPosX() {
        return posX;
    }

    public float getPosY() {
        return posY;
    }

    @Override
    public float getWidth() {
        return width;
    }

    @Override
    public float getHeight() {
        return height;
    }

    public BaseActor getFeetSensor() {
        return feetSensor;
    }

    public BaseActor getGrabSensor() {
        return grabSensor;
    }

    public void setGravityEnabled(boolean gravityEnabled) {
        this.gravityEnabled = gravityEnabled;
    }

    public Vector2 getVelocityVector(){
        return velocityVec;
    }

    public void setBigCrate(boolean bigCrate) {
        this.bigCrate = bigCrate;
        setAnimation(loadTexture("src/apalacios/pfg/assets/world_elements/caja_stand_big.png"));
    }

    public void move(float accelerationX, float accelerationY) {
        accelerationVec.x = 0;
        if (accelerationX < 0)
            direction = Direction.RIGHT;
        else if (accelerationX > 0)
            direction = Direction.LEFT;
        this.accelerationX = accelerationX;
        this.accelerationY = accelerationY;
        moving = true;
    }

    public void reciveKick(Direction direction) {
        this.direction = direction;
        gravityEnabled = true;
        moving = true;

        accelerationVec.x = 0;
        if (direction == Direction.RIGHT) {
            accelerationX = 25;
        } else {
            accelerationX = -25;
        }
        accelerationY = 7.5f;
    }

    public void drop(Direction direction) {
        this.direction = direction;
        moving = true;

        accelerationVec.x = 0;
        if (direction == Direction.RIGHT) {
            accelerationX = 50;
        } else {
            accelerationX = -50;
        }
        accelerationY = 60;
    }

    public void destroy() {
        if (alive) {
            alive = false;
            feetSensor.remove();
            grabSensor.remove();
            if (bigCrate) {
                crateDestroyed.setAnimation(loadAnimationFromSheet("src/apalacios/pfg/assets/world_elements/" +
                        "caja_break_big.png", 1, 6, 0.05f, true));
                crateDestroyed.centerAtActor(this);
            } else {
                crateDestroyed.setAnimation(loadAnimationFromSheet("src/apalacios/pfg/assets/world_elements/" +
                        "caja_break.png", 1, 6, 0.05f, true));
                crateDestroyed.centerAtActor(this);
            }
            crateDestroyed.addAction(Actions.delay(0.2f));
            crateDestroyed.addAction(Actions.after(Actions.removeActor()));
        }
    }

    @Override
    public void trampolineJump() {
        gravityEnabled = true;
        setY(getY() + 30);
        move(0, 120);
    }

    @Override
    public void act(float dt) {
        super.act(dt);

        if (alive) {
            // Aceleración y deceleración
            if (moving) {
                accelerationVec.add(accelerationX, accelerationY);
                moving = false;
            } else {
                if (direction == Direction.RIGHT) {
                    accelerationVec.add(-horizontalDeceleration, 0);
                    if (velocityVec.x < 0) {
                        accelerationVec.x = 0;
                        velocityVec.x = 0;
                    }
                } else {
                    accelerationVec.add(horizontalDeceleration, 0);
                    if (velocityVec.x > 0) {
                        accelerationVec.x = 0;
                        velocityVec.x = 0;
                    }
                }
            }

            // Simular gravedad
            if (gravityEnabled) {
                accelerationVec.add(0, -gravity);
            } else {
                accelerationVec.y = 0;
                velocityVec.y = 0;
            }

            velocityVec.add(accelerationVec.x, accelerationVec.y);

            velocityVec.x = MathUtils.clamp(velocityVec.x, -maxHorizontalSpeed, maxHorizontalSpeed);
            velocityVec.y = MathUtils.clamp(velocityVec.y, -maxFallSpeed, maxFallSpeed);

            moveBy(Math.round(velocityVec.x * dt), Math.round(velocityVec.y * dt));

            // Alinear sensor
            feetSensor.centerAtPosition(getX() + 18, getY() + 1);
            grabSensor.centerAtActor(this);

            crateDestroyed.centerAtActor(this);
        } else {
            remove();
        }
    }
}
