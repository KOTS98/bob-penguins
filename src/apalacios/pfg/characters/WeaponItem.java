package apalacios.pfg.characters;

import apalacios.pfg.util.Direction;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class WeaponItem extends Mobile {
    private float posX;
    private float posY;

    // Físicas
    private float accelerationX;
    private float accelerationY;
    private float maxHorizontalSpeed;
    private float horizontalDeceleration;
    private float gravity;
    private boolean gravityEnabled;
    private float maxFallSpeed;

    private boolean moving;
    private Direction direction;
    private BaseActor feetSensor;
    private BaseActor grabSensor;

    // Texturas
    private int nWeapon;
    private BaseActor body;
    private Animation right;
    private Animation left;

    // Munición
    private int nMunition;

    public WeaponItem(float posX, float posY, Stage stage, int nWeapon, int nMunition) {
        super(posX, posY, stage);
        setName("weapon");
        this.nWeapon = nWeapon;
        this.nMunition = nMunition;

        this.posX = posX;
        this.posY = posY;

        body = new BaseActor(getX(), getY(), getStage());
        right = body.loadTexture("src/apalacios/pfg/assets/weapons/ar" + nWeapon + "/right_item.png");
        left = body.loadTexture("src/apalacios/pfg/assets/weapons/ar" + nWeapon + "/left_item.png");
        body.setAnimation(right);

        setSize(40, 40);
        setBoundaryRectangle();

        // Físicas
        horizontalDeceleration = 2;
        maxHorizontalSpeed = 2000;
        gravity = 5;
        gravityEnabled = true;
        maxFallSpeed = 800;

        moving = false;
        direction = Direction.RIGHT;
        feetSensor = new BaseActor(getX(), getY(), getStage());
        feetSensor.setSize(11, 5);
        feetSensor.setBoundaryRectangle();

        grabSensor = new BaseActor(getX(), getY(), getStage());
        grabSensor.setSize(55, 40);
        grabSensor.setBoundaryRectangle();
    }

    public float getPosX() {
        return posX;
    }

    public float getPosY() {
        return posY;
    }

    public int getNWeapon() {
        return nWeapon;
    }

    public BaseActor getFeetSensor() {
        return feetSensor;
    }

    public BaseActor getGrabSensor() {
        return grabSensor;
    }

    public void setGravityEnabled(boolean gravityEnabled) {
        this.gravityEnabled = gravityEnabled;
    }

    public Vector2 getVelocityVector() {
        return velocityVec;
    }

    public int getnMunition() {
        return nMunition;
    }

    public void setVelocityZero() {
        this.accelerationVec.x = 0;
        this.accelerationVec.y = 0;
        this.velocityVec.x = 0;
        this.velocityVec.y = 0;
    }

    void drop(Direction direction) {
        this.direction = direction;
        moving = true;

        accelerationVec.x = 0;
        if (direction == Direction.RIGHT) {
            accelerationX = 30;
            body.setAnimation(right);
        } else {
            accelerationX = -30;
            body.setAnimation(left);
        }
        accelerationY = 15;
    }

    @Override
    public void trampolineJump() {
        gravityEnabled = true;
        setY(getY() + 30);
        this.accelerationY = 120;
        moving = true;
    }

    @Override
    public boolean remove() {
        super.remove();
        grabSensor.remove();
        feetSensor.remove();
        body.remove();
        return false;
    }

    @Override
    public void act(float dt) {
        super.act(dt);

        // Aceleración y deceleración
        if (moving) {
            accelerationVec.add(accelerationX, accelerationY);
            moving = false;
        } else {
            if (direction == Direction.RIGHT) {
                accelerationVec.add(-horizontalDeceleration, 0);
                if (velocityVec.x < 0) {
                    accelerationVec.x = 0;
                    velocityVec.x = 0;
                }
            } else {
                accelerationVec.add(horizontalDeceleration, 0);
                if (velocityVec.x > 0) {
                    accelerationVec.x = 0;
                    velocityVec.x = 0;
                }
            }
        }

        // Simular gravedad
        if (gravityEnabled) {
            accelerationVec.add(0, -gravity);
        } else {
            accelerationVec.y = 0;
            velocityVec.y = 0;
        }

        velocityVec.add(accelerationVec.x, accelerationVec.y);

        velocityVec.x = MathUtils.clamp(velocityVec.x, -maxHorizontalSpeed, maxHorizontalSpeed);
        velocityVec.y = MathUtils.clamp(velocityVec.y, -maxFallSpeed, maxFallSpeed);

        moveBy(Math.round(velocityVec.x * dt), Math.round(velocityVec.y * dt));

        // Alinear sensor
        feetSensor.centerAtPosition(getX() + (getWidth() / 2), getY() - 13);
        body.centerAtPosition(getX() + (getWidth() / 2), getY());
        grabSensor.centerAtActor(this);
    }
}