package apalacios.pfg.characters;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.util.Random;

public class WeaponSpawner extends BaseActor{

    // Animación
    private Animation standAnimation;
    private Animation activeAnimation;
    private enum AnimState {
        STAND, ACTIVE
    }
    private AnimState animState;

    // Generar arma
    private float newWeaponTimer;

    private boolean hasWeapon;
    private WeaponItem currentWeapon;

    public WeaponSpawner(float posX, float posY, Stage stage) {
        super(posX, posY, stage);

        standAnimation = loadTexture("src/apalacios/pfg/assets/world_elements/spawner_stand.png");
        activeAnimation = loadAnimationFromSheet("src/apalacios/pfg/assets/world_elements/spawner_anim.png",
                1, 4, 0.1f, true);
        setAnimation(activeAnimation);
        animState = AnimState.ACTIVE;
        newWeaponTimer = 0;

        generateWeapon();
    }

    public boolean itHasWeapon() {
        return hasWeapon;
    }

    public void setWeapon(WeaponItem weaponItem) {
        this.currentWeapon = weaponItem;
        currentWeapon.setVelocityZero();
        currentWeapon.setGravityEnabled(false);
        currentWeapon.setPosition(getX() + (getWidth() / 2) - (currentWeapon.getWidth() / 2), getY() + 20);
        currentWeapon.addAction(Actions.forever(Actions.sequence(Actions.moveBy(0, 10, 0.5f),
                Actions.moveBy(0, -10, 0.5f))));
        hasWeapon = true;
        newWeaponTimer = 0;
    }

    public void removeWeapon() {
        hasWeapon = false;
        currentWeapon = null;
    }

    private void generateWeapon() {
        hasWeapon = true;
        newWeaponTimer = 0;
        currentWeapon = new WeaponItem(getX(), getY(), getStage(), new Random().nextInt(7) + 2, -1); //new Random().nextInt(7) + 2)
        currentWeapon.setGravityEnabled(false);
        currentWeapon.setPosition(getX() + (getWidth() / 2) - (currentWeapon.getWidth() / 2), getY() + 20);
        currentWeapon.addAction(Actions.forever(Actions.sequence(Actions.moveBy(0, 10, 0.5f),
                Actions.moveBy(0, -10, 0.5f))));
    }

    @Override
    public void act(float dt) {
        super.act(dt);

        if (hasWeapon) {
            if (animState == AnimState.STAND) {
                setAnimation(activeAnimation);
                animState = AnimState.ACTIVE;
            }
        } else {
            if (animState == AnimState.ACTIVE) {
                setAnimation(standAnimation);
                animState = AnimState.STAND;
            }
            newWeaponTimer += dt;
            if (newWeaponTimer >= 10)
                generateWeapon();
        }
    }
}
